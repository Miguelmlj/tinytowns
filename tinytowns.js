// genera numero de 1 a 15 para el mazo de robo
function generaNumero(){
    var min = Math.ceil(1);
    var max = Math.floor(5);
    
    return Math.floor(Math.random() * (1 + max - min) + min);
   
}

// función para obtener los recursos después de barajear mazoderobo
function dameRecursos(){
var Madera =1;
var Trigo =2;
var Ladrillo =3;
var Vidrio =4;
var Piedra =5;
var mad=0;
var tri=0;
var lad=0;
var vid=0;
var pie=0;
    
var mazoderobo = []; 
var carta = '', carta1 = '', carta2 = '';    
var construe = false;
    
while(mazoderobo.length != 15){
    
var numaleatorio = generaNumero();
    
if((numaleatorio == Madera) && (mad <=2)){
    
    mazoderobo.push('Madera');
    mad++;
    
}else if((numaleatorio == Trigo) && (tri <=2)){    
    
    mazoderobo.push('Trigo');
    tri++;

}else if((numaleatorio == Ladrillo) && (lad<=2)){    
    
    mazoderobo.push('Ladrillo');
    lad++;
}else if((numaleatorio == Vidrio) && (vid<=2)){    
    
    mazoderobo.push('Vidrio');
    vid++;
}else{    
    if(pie<=2){
    mazoderobo.push('Piedra');
    pie++;
}}
    
    
}
  
    return mazoderobo;
    
}    


//pinta el campo DIV de los recursos
function colorRecurso(mazorobo){
 for(var j=0; j<=2; j++){            
                
           var rec = document.getElementById('recurso'+j+'');
            
            if(mazorobo[j] == 'Madera'){
                rec.style.backgroundColor = '#9C690A';
            }else if(mazorobo[j] == 'Trigo'){
                rec.style.backgroundColor = '#D3CA0C';
                
            }else if(mazorobo[j] == 'Ladrillo'){
                rec.style.backgroundColor = '#D67412';
                
            }else if(mazorobo[j] == 'Vidrio'){
                rec.style.backgroundColor = '#AADBDF';
                
            }else if(mazorobo[j] == 'Piedra'){
                rec.style.backgroundColor = '#53686A';
                
            }else{}
            
            
            }

}

//variables globales
 var color='';
 var recursoUtilizado = '';
 var tomasteRecurso = false;
 var recursoEnTablero = '';
 var itemtablero = '';
 var recursoConstruye = ''; //establecer edificio.
 var cons = 0; // variable para confirmar construcción cast.
 var cdr = 1;
 

//función para ver que recurso captura el usuario
 function tomarRecurso(recurso){
   
     var valorDeRec = document.getElementById(recurso).innerHTML;
     
     //esta variable la utilizaremos en otra función()
     recursoUtilizado = recurso;
     tomasteRecurso = true;
     
     //Color de recurso
    if(valorDeRec == 'Madera'){
        
        color = '#9C690A';
        
    }else if(valorDeRec == 'Trigo'){
        
        color = '#D3CA0C';
        
    }else if(valorDeRec == 'Ladrillo'){
        
        color = '#D67412';
        
    }else if(valorDeRec == 'Vidrio'){
        
        color = '#AADBDF';
        
    }else if(valorDeRec == 'Piedra'){
        
        color = '#53686A';
        
    }else{}
    
     
    return color;
    
}

function hoverin(item){
    var clic = document.getElementById(item);
    clic.style.border= "2px solid white";
}

function hoverout(item){
    var clic = document.getElementById(item);
    clic.style.border= "2px solid blue";
}

//¿QUE OBJETO(ITEM) ME ESTÁ ENVIANDO EL EVENTO?
// ¿QUE OBJETO(ITEM) MANDA LA SEÑAL? ¿COMO LO IDENTIFICO?

function PonerRecursoEnTablero(item){
    var cuadrante = document.getElementById(item);
   cons++;
    if(cons >= 2){
        if(cons == 2){
            ConstruirPozo(item);
           
        }else if(cons == 3){
                 
            ConstruirCabaña(item);         
        }else if(cons == 4){
                 
            ConstruirGranja(item);     
                 
        }else if(cons == 5){
                 
            ConstruirAsilo(item);     
                 
        }else if(cons == 6){
                 
            ConstruirCapilla(item);     
                 
        }else if(cons == 7){
                 
            ConstruirFabrica(item);     
                 
        }else if(cons == 8){
                 
               ConstruirPasteleria(item);  
        }else{}
        
        cons = 0;
        
    }else{
    
        var cuadranteOcupado = document.getElementById(item).innerHTML;
          
    //-------------------------------------------------------
            if(cuadranteOcupado == ""){
            if(tomasteRecurso){        
       
            cuadrante.style.backgroundColor = color;
            obtenerNombreDelRecurso();
            cuadrante.innerHTML = recursoEnTablero;
            operacionesTablero(item);
            color = '';
            actualizarMazo();  
                
            if(TableroLLeno()){                 
                if(!PuedoConstruir()){                
                DamePuntajeFinal();   
                    
                }else{ alert('Puede construir edificios.');}
            }else{}
              
                
            }else{
            alert("Tome un recurso y vuelva a intentarlo.");
            }
    //-------------------------------------------------------  
                
            }else if(cuadranteOcupado == 'Fabrica'){
               
                almacenarRecursoEnFabrica(item);
                  
    //-------------------------------------------------------
            }else{
            
            alert("Cuadrante Ocupado");
            }
        
    }
     if(ref[0] != ''){
            BonusFabrica(ref);
        }else{}
        
    cons = 0;
    
}

//abrir panel para obtener los 5 recursos
function BonusFabrica(almacen){
    
        var uno = document.getElementById('recurso0').innerHTML;
        var dos = document.getElementById('recurso1').innerHTML;
        var tres = document.getElementById('recurso2').innerHTML;
    
    for(var j=0; j<=almacen.length-1; j++){
        
        if(almacen[j] == uno || almacen[j] == dos || almacen[j] == tres){
            
            for(var i=0; i<=4; i++){
            document.getElementById('recursof'+i+'').style.display = 'block';
            } 
            
            break;
        }else{
            for(var i=0; i<=4; i++){
            document.getElementById('recursof'+i+'').style.display = 'none';
            } break;
            
        }
        
       }
    
}
//reset
function reiniciar(){
    for(var i=1; i<=16; i++){
        document.getElementById('item'+i+'').style.background = '#F0E68C';
        
        document.getElementById('item'+i+'').innerHTML = '';
        
    }
    
    for(var i=0; i<=3; i++){
        for(var l=0; l<=3; l++){
           tablero[i][l] = '0';
            
        }
    }
    
    recursoEnFabrica = '', llavef = false;
    for(var i=0; i<=ref.length-1; i++){
        ref[i] = '';
        
    }
    
    for(var j=0; j<=refitem.length-1; j++){
        refitem[j] = '';
        
    }
    
    cdr = 1;
    
    document.getElementById('rpuntaje').style.border = 'none';
    document.getElementById('rne').style.border = 'none';
    
    document.getElementById('rpuntaje').innerHTML = ''
    document.getElementById('rne').innerHTML = '';
    
    for(var i=0; i<=4; i++){
            document.getElementById('recursof'+i+'').style.display = 'none';
    }
    
}
    //obtener el puntaje final
function DamePuntajeFinal(){

    var tf = 0, tc = 0;
    for(var i=1; i<=4; i++){
        for(var c=1; c<=4; c++){
            
        puntaje[i][c] = tablero[tf][tc];    
        tc++;    
        }
        tf++; tc=0;
    }
    
    //=======================
    var alimento_granja = 0; // 4 alimentos por cada granja
    var cabaña_alimentada = 0; // 3 puntos por cada una
    var pozo_cabaña_adyacente = 0; // 1 punto por cada poz ady
    var capilla_cabaña_alimenta = 0; //igual a # cabañas alim.
    var puntuacion_final = 0;
    var pasteleria_lateral_fabgran = 0; //3 puntos si true
    var asilo_puntuacion = 0; // depende del número de ellos
    var nexperiencia = '';
    var puntosmenos=0;
    var chec = '';
    //======BUSCAR EDIFICIO granja
    for(var i=1; i<=4; i++){
        for(var c=1; c<=4; c++){
            
        if(puntaje[i][c] == 'Granja'){
            alimento_granja +=4; // almacena 4 alimentos en var.
        }else{}   
            
        }
        
    }
    
    //======BUSCAR EDIFICIO cabaña
    for(var i=1; i<=4; i++){
        for(var c=1; c<=4; c++){
            
        if(puntaje[i][c] == 'Cabaña'){
           
            if(alimento_granja >=1){
             cabaña_alimentada++;
             alimento_granja--; // reduce 1 alimento
        }else{}   
            
        }else{}
        
    }
    }
    
    //======BUSCAR EDIFICIO capilla
    for(var i=1; i<=4; i++){
        for(var c=1; c<=4; c++){
            
        if(puntaje[i][c] == 'Capilla'){
           
            capilla_cabaña_alimenta = cabaña_alimentada;
            
        }else{}
        
    }
    }
    
    
    //==============BUSCAR EDIFICIO pozo
    for(var i=1; i<=4; i++){
        for(var c=1; c<=4; c++){
            
        if(puntaje[i][c] == 'Pozo'){
           
        if(puntaje[i][c-1] == 'Cabaña'){
            pozo_cabaña_adyacente++;
        }else if(puntaje[i-1][c] == 'Cabaña'){
            pozo_cabaña_adyacente++;     
        }else if(puntaje[i][c+1] == 'Cabaña'){
            pozo_cabaña_adyacente++;     
        }else if(puntaje[i+1][c] == 'Cabaña'){
            pozo_cabaña_adyacente++;     
        }else{}
            
            
        }else{}
        
    }
    }
    
    //==============BUSCAR EDIFICIO Pateleria
    for(var i=1; i<=4; i++){
        for(var c=1; c<=4; c++){
            
        if(puntaje[i][c] == 'Pasteleria'){
           
        if(puntaje[i][c-1] == 'Granja' || puntaje[i][c-1] == 'Fabrica'){
           pasteleria_lateral_fabgran++;
        }else if(puntaje[i][c+1] == 'Granja' || puntaje[i][c+1] == 'Fabrica'){
            pasteleria_lateral_fabgran++;     
                 
        }else{}
            
        }else{}
        
    }
    }
    
    //=============buscar asilo
    for(var i=1; i<=4; i++){
        for(var c=1; c<=4; c++){
        if(puntaje[i][c] == 'Asilo'){
        asilo_puntuacion++;
        }else{}
    }
    }    
    
    //===========puntos menos
    for(var i=1; i<=16; i++){
        chec = document.getElementById('item'+i+'').innerHTML;
        if(chec == 'Madera' || chec == 'Vidrio' || chec == 'Piedra' || chec == 'Ladrillo' || chec == 'Trigo'){
            
            puntosmenos-=1;
        }else{}
        
    }
   
    //=======operación asilo
    if(asilo_puntuacion == 1){
        asilo_puntuacion = -1;
    }else if(asilo_puntuacion == 3){
        asilo_puntuacion = -3;     
    }else if(asilo_puntuacion == 5){
        asilo_puntuacion = -5;     
    }else if(asilo_puntuacion == 2){
        asilo_puntuacion = 5;     
    }else if(asilo_puntuacion == 4){
        asilo_puntuacion = 15;     
    }else if(asilo_puntuacion == 6){
        asilo_puntuacion = 26;     
    }else{
        asilo_puntuacion = 0;
    }
    
    
    puntuacion_final = (cabaña_alimentada * 3) + capilla_cabaña_alimenta + pozo_cabaña_adyacente + (pasteleria_lateral_fabgran * 3) + asilo_puntuacion;
    
    puntuacion_final = puntuacion_final+puntosmenos; 
    
    if(puntuacion_final <= 9){
        nexperiencia = 'Aspirante a Arquitecto';
        
    }else if(puntuacion_final >= 10 && puntuacion_final <= 17){
             nexperiencia = 'Aprendiz de Constructor';
    }else if(puntuacion_final >= 18 && puntuacion_final <= 24){
             nexperiencia = 'Carpintero';
    }else if(puntuacion_final >= 25 && puntuacion_final <= 31){
             nexperiencia = 'Ingeniero';
    }else if(puntuacion_final >= 32 && puntuacion_final <= 37){
             nexperiencia = 'Urbanista';
    }else if(puntuacion_final >= 38){
             nexperiencia = 'Maestro Arquitecto';
    }
    
    //alert('su puntuación final es: '+puntuacion_final);
    document.getElementById('rpuntaje').innerHTML = puntuacion_final;
    document.getElementById('rpuntaje').style.border = '2px dotted #79F810';
    
    document.getElementById('rne').innerHTML = nexperiencia;
    document.getElementById('rne').style.border = '2px dotted #79F810';
    
    alert('Partida Finalizada, Favor de Reiniciar para comenzar una nueva partida.')
    
}
//verificar si el tablero está completo 
function TableroLLeno(){
    
    var tablerolleno = true;
    var cuadro = '';
    for(var i=1; i<=16; i++){
        
        cuadro = document.getElementById('item'+i+'').innerHTML;
        
        if(cuadro == ''){
           tablerolleno = false;
           
        }else{}
        
    }
    
    return tablerolleno;
    
}

function almacenarRecursoEnFabrica(item){
                obtenerNombreDelRecurso();
                
        if(tomasteRecurso){
            for(var i=0; i<=ref.length-1; i++){
                    
            if(ref[i] == 'Madera' || ref[i] == 'Vidrio' || ref[i] == 'Piedra' || ref[i] == 'Ladrillo' || ref[i] == 'Trigo'){
                        
            if(refitem[i] == item){
                    alert('Se encuentra un recurso almacenado en esta Fábrica.'); break;
                        
            }else{
                        
                if(refitem[i+cdr] == item){
                    alert('Se encuentra un recurso almacenado en esta Fábrica.'); break;
                }else{
                    ref[i+cdr] = recursoEnTablero; refitem[i+cdr] = item; alert('Se almacenó recurso '+recursoEnTablero+' en Fábrica'); break;
                    cdr++;
                    }
                }
                }else{
                        
                    ref[i] = recursoEnTablero; refitem[i] = item; alert('Se almacenó recurso '+recursoEnTablero+' en Fábrica'); break;
                    }
                }
                }else{alert('Tome un recurso para almacenarlo en fábrica.');
                }
    actualizarMazo();
    
}
// verificar si hay patrones para construir
function PuedoConstruir(){
    var patronDisponible = false;
    abe = false;
    var T = 'Trigo', L = 'Ladrillo', V = 'Vidrio', M = 'Madera', P = 'Piedra';
    //==========COTTAGE=============//
    for(var p=1; p<=3; p++){
            for(var k=0; k<=2; k++){
             
            if(tablero[p-1][k+1] == T && tablero[p][k] == L && tablero[p][k+1] == V){
                patronDisponible = true;  
            }else if(tablero[p-1][k] == T && tablero[p][k] == V && tablero[p][k+1] == L){
                patronDisponible = true; 
            }else if(tablero[p-1][k] == V && tablero[p-1][k+1] == L && tablero[p][k] == T){
                patronDisponible = true; 
            }else if(tablero[p-1][k] == L && tablero[p-1][k+1] == V && tablero[p][k+1] == T){
                patronDisponible = true; 
            }else if(tablero[p-1][k+1] == L && tablero[p][k] == T && tablero[p][k+1] == V){
                patronDisponible = true; 
            }else if(tablero[p-1][k] == L && tablero[p][k] == V && tablero[p][k+1] == T){
                patronDisponible = true; 
            }else if(tablero[p-1][k] == T && tablero[p-1][k+1] == V && tablero[p][k+1] == L){
                patronDisponible = true; 
            }else if(tablero[p-1][k] == V && tablero[p-1][k+1] == T && tablero[p][k] == L){
                patronDisponible = true; 
            }else{}
            }
        }
    //==========CAPILLA=============//
        for(var p=1; p<=3; p++){
        for(var k=0; k<=1; k++){
            if(tablero[p-1][k+2] == V && tablero[p][k] == P && tablero[p][k+1] == V && tablero[p][k+2] == P){
                
                patronDisponible = true;
            }else if(tablero[p-1][k] == V && tablero[p][k] == P && tablero[p][k+1] == V && tablero[p][k+2] == P){
                     
                 patronDisponible = true;
            }else if(tablero[p-1][k] == P && tablero[p-1][k+1] == V && tablero[p-1][k+2] == P && tablero[p][k] == V){
                     
                 patronDisponible = true;
            }else if(tablero[p-1][k] == P && tablero[p-1][k+1] == V && tablero[p-1][k+2] == P && tablero[p][k+2] == V){
                     
                 patronDisponible = true;
            }else{}
        }
    }
    
        for(var i=0; i<=1; i++){
        for(var l=1; l<=3; l++){
           
         if(tablero[i][l] == P && tablero[i+1][l] == V && tablero[i+2][l] == P && tablero[i+2][l-1] == V){
                     
                     patronDisponible = true;
            }else if(tablero[i][l-1] == P && tablero[i+1][l-1] == V && tablero[i+2][l-1] == P && tablero[i+2][l] == V){
                     
                     patronDisponible = true;
            }else if(tablero[i][l] == P && tablero[i+1][l] == V && tablero[i+2][l] == P && tablero[i][l-1] == V){
                     
                     patronDisponible = true;
            }else if(tablero[i][l-1] == P && tablero[i+1][l-1] == V && tablero[i+2][l-1] == P && tablero[i][l] == V){
                     
                     patronDisponible = true;
            }else{}
        }
    }
    //==========POZO=============//

        for(var i=0; i<=3; i++){
        for(var j=0; j<=2; j++){
            if(tablero[i][j] == M && tablero[i][j+1] == P){
             patronDisponible = true;
            }else if(tablero[i][j] == P && tablero[i][j+1] == M){
                     patronDisponible = true;
            }else{}
        }
        }
    
        for(var i=0; i<=2; i++){
        for(var j=0; j<=3; j++){
            if(tablero[i][j] == M && tablero[i+1][j] == P){
                patronDisponible = true;
            }else if(tablero[i][j] == P && tablero[i+1][j] == M){
                patronDisponible = true;
            }else{}
        }
        }
    //==========PASTELERIA=============//
    for(var p=1; p<=3; p++){
        for(var k=0; k<=1; k++){
    if(tablero[p-1][k+1] == T && tablero[p][k] == L && tablero[p][k+1] == V && tablero[p][k+2] == L){
        patronDisponible = true;
       }else if(tablero[p-1][k] == L && tablero[p-1][k+1] == V && tablero[p-1][k+2] == L && tablero[p][k+1] == T){
                   patronDisponible = true;
            }else{}
        }
    }
    
    for(var i=0; i<=1; i++){
        for(var l=1; l<=3; l++){
             if(tablero[i][l] == L && tablero[i+1][l] == V && tablero[i+2][l] == L && tablero[i+1][l-1] == T){
                     patronDisponible = true;
            }else if(tablero[i][l-1] == L && tablero[i+1][l-1] == V && tablero[i+2][l-1] == L && tablero[i+1][l] == T){
                     patronDisponible = true;
            }else{}
        }
    }
    //==========ASILO=============//
        for(var i=0; i<=3; i++){
        for(var j=0; j<=1; j++){
            if(tablero[i][j] == P && tablero[i][j+1] == P && tablero[i][j+2] == V){
             patronDisponible = true;
            }else if(tablero[i][j] == V && tablero[i][j+1] == P && tablero[i][j+2] == P){
                 patronDisponible = true;    
            }else{}
        }
    }
        for(var i=0; i<=1; i++){
        for(var j=0; j<=3; j++){
            if(tablero[i][j] == V && tablero[i+1][j] == P && tablero[i+2][j] == P){
                patronDisponible = true;
            }else if(tablero[i][j] == P && tablero[i+1][j] == P && tablero[i+2][j] == V){
                patronDisponible = true;     
            }else{}
        }
    }
    
    //=========GRANJA=============//
            for(var p=1; p<=3; p++){
            for(var k=0; k<=2; k++){
                if(tablero[p-1][k] == T && tablero[p-1][k+1] == T && tablero[p][k] == M && tablero[p][k+1] == M){
                        patronDisponible = true;
                }else if(tablero[p-1][k] == T && tablero[p-1][k+1] == M && tablero[p][k] == T && tablero[p][k+1] == M){
                        patronDisponible = true;
                }else if(tablero[p-1][k] == M && tablero[p-1][k+1] == M && tablero[p][k] == T && tablero[p][k+1] == T){
                        patronDisponible = true;
                }else if(tablero[p-1][k] == M && tablero[p-1][k+1] == T && tablero[p][k] == M && tablero[p][k+1] == T){
                       patronDisponible = true;
                }else{}
            }
        }
    
    //=============FABRICA=============// 
    for(var p=1; p<=3; p++){
        var k= 0;
            if(tablero[p-1][k] == M && tablero[p][k] == L && tablero[p][k+1] == P && tablero[p][k+2] == P && tablero[p][k+3] == L){
               patronDisponible = true;
              
            }else if(tablero[p-1][k+3] == M && tablero[p][k] == L && tablero[p][k+1] == P && tablero[p][k+2] == P && tablero[p][k+3] == L){
                   patronDisponible = true; 
                    
            }else if(tablero[p-1][k] == L && tablero[p-1][k+1] == P && tablero[p-1][k+2] == P && tablero[p-1][k+3] == L && tablero[p][k+3] == M){
                   patronDisponible = true; 
                    
            }else if(tablero[p-1][k] == L && tablero[p-1][k+1] == P && tablero[p-1][k+2] == P && tablero[p-1][k+3] == L && tablero[p][k] == M){
                   patronDisponible = true; 
                     
            }else{}
    
        }
    
        for(var l=1; l<=3; l++){
           var i=0;
           
             if(tablero[i][l] == L && tablero[i+1][l] == P && tablero[i+2][l] == P && tablero[i+3][l] == L && tablero[i+3][l-1] == M){
                     patronDisponible = true;
                  
            }else if(tablero[i][l] == L && tablero[i+1][l] == P && tablero[i+2][l] == P && tablero[i+3][l] == L && tablero[i][l-1] == M){
                     patronDisponible = true;
                   
            }else if(tablero[i][l-1] == L && tablero[i+1][l-1] == P && tablero[i+2][l-1] == P && tablero[i+3][l-1] == L && tablero[i][l] == M){
                     patronDisponible = true;
                       
            }else if(tablero[i][l-1] == L && tablero[i+1][l-1] == P && tablero[i+2][l-1] == P && tablero[i+3][l-1] == L && tablero[i+3][l] == M){
                     patronDisponible = true;
                     
            }else{}
            
        }    
    
    
    return patronDisponible;
}

function ConstruirPozo(item){
    var M = 'Madera';
    var P = 'Piedra';
    var a = 1;
    var sa = false, sb = false, sc = false, sd = false; 
    var chec =0; // checará si entro en algún patrón
    //==============================================PATRON 1
    var cv = 0;
        for(var i=0; i<=3; i++){
        
        for(var j=0; j<=2; j++){
            if(tablero[i][j] == M && tablero[i][j+1] == P){
                
                
                if(item == 'item'+a+''){ 
                    sa = true;
                    chec++;
    document.getElementById('item'+a+'').style.border = '2px solid blue';
    document.getElementById('item'+a+'').innerHTML = 'Pozo';       document.getElementById('item'+a+'').style.background = '#751C0E';   tablero[i][j] = 'Pozo';       
                    
                    
    document.getElementById('item'+(a+1)+'').style.border = '2px solid blue';                
    document.getElementById('item'+(a+1)+'').innerHTML = ''; //valia p
    document.getElementById('item'+(a+1)+'').style.background = '#F0E68C';
    tablero[i][j+1] = '0'; //valia P              

                
                }else if(item == 'item'+(a+1)+''){  
                    sa = true;
                    chec++;
    document.getElementById('item'+(a+1)+'').style.border = '2px solid blue';                
    document.getElementById('item'+(a+1)+'').innerHTML = 'Pozo';   document.getElementById('item'+(a+1)+'').style.background = '#751C0E';
    tablero[i][j+1] = 'Pozo';                
                    
    
    document.getElementById('item'+a+'').style.border = '2px solid blue';
    document.getElementById('item'+a+'').innerHTML = '';           document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[i][j] = '0';  
                    
                }else{
                    
                   cv++;
                    //switcha = true;
                }
                
            }else{}
            a++;
        
        }
            a++;
        
    }   
    
    //==============================================PATRON 2
    a = 1;
        if(chec == 0){
        for(var i=0; i<=3; i++){
        
        for(var j=0; j<=2; j++){
        if(tablero[i][j] == P && tablero[i][j+1] == M){
                
                if(item == 'item'+a+''){ 
                    sb = true;
                    chec++;
    document.getElementById('item'+a+'').style.border = '2px solid blue';
    document.getElementById('item'+a+'').innerHTML = 'Pozo';       document.getElementById('item'+a+'').style.background = '#751C0E';   tablero[i][j] = 'Pozo';       
                    
                    
    document.getElementById('item'+(a+1)+'').style.border = '2px solid blue';                
    document.getElementById('item'+(a+1)+'').innerHTML = ''; //valia p
    document.getElementById('item'+(a+1)+'').style.background = '#F0E68C';
    tablero[i][j+1] = '0'; //valia P              

                
                }else if(item == 'item'+(a+1)+''){  
                  sb = true;
                    chec++;
    document.getElementById('item'+(a+1)+'').style.border = '2px solid blue';                
    document.getElementById('item'+(a+1)+'').innerHTML = 'Pozo';   document.getElementById('item'+(a+1)+'').style.background = '#751C0E';
    tablero[i][j+1] = 'Pozo';                
                    
    
    document.getElementById('item'+a+'').style.border = '2px solid blue';
    document.getElementById('item'+a+'').innerHTML = '';           document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[i][j] = '0';  
                    
                }else{cv++; //switchb = true;
                     }
                
            }else{}
            a++;
            
    
        
        }
            a++;
       }
    }else{}  
    
    
//==========================================PATRON 3 
    a = 1;
    if(chec == 0){
    for(var i=0; i<=2; i++){
        
        for(var j=0; j<=3; j++){
            if(tablero[i][j] == M && tablero[i+1][j] == P){
        
                if(item == 'item'+a+''){
                    chec++;
                    sc = true;
     document.getElementById('item'+a+'').style.border = '2px solid blue';
    document.getElementById('item'+a+'').innerHTML = 'Pozo';       document.getElementById('item'+a+'').style.background = '#751C0E';   tablero[i][j] = 'Pozo';                 
                    
                    
     document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';                
    document.getElementById('item'+(a+4)+'').innerHTML = ''; //valia p
    document.getElementById('item'+(a+4)+'').style.background = '#F0E68C';
    tablero[i+1][j] = '0'; 
                    
                }else if(item == 'item'+(a+4)+''){
                chec++;
                sc = true;
     document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';                
    document.getElementById('item'+(a+4)+'').innerHTML = 'Pozo';   document.getElementById('item'+(a+4)+'').style.background = '#751C0E';
    tablero[i+1][j] = 'Pozo';                 
                    
    
    document.getElementById('item'+a+'').style.border = '2px solid blue';
    document.getElementById('item'+a+'').innerHTML = '';           document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[i][j] = '0';                 
                    
                    
            }else{cv++; //switchc = true;
                 }
        }else{}
            a++;
        }
            //a++;
    
    }
        }else{}
    
    
    
    //==========================================PATRON 4
    a = 1;
    if(chec == 0){
    for(var i=0; i<=2; i++){
        
        for(var j=0; j<=3; j++){   
            if(tablero[i][j] == P && tablero[i+1][j] == M){
    
            if(item == 'item'+a+''){
                    //chec++;
                sd = true;
     document.getElementById('item'+a+'').style.border = '2px solid blue';
    document.getElementById('item'+a+'').innerHTML = 'Pozo';       document.getElementById('item'+a+'').style.background = '#751C0E';   tablero[i][j] = 'Pozo';                 
                    
                    
     document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';                
    document.getElementById('item'+(a+4)+'').innerHTML = ''; //valia p
    document.getElementById('item'+(a+4)+'').style.background = '#F0E68C';
    tablero[i+1][j] = '0'; 
                    
                }else if(item == 'item'+(a+4)+''){
                //chec++;
                sd = true;
     document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';                
    document.getElementById('item'+(a+4)+'').innerHTML = 'Pozo';   document.getElementById('item'+(a+4)+'').style.background = '#751C0E';
    tablero[i+1][j] = 'Pozo';                 
                    
    
    document.getElementById('item'+a+'').style.border = '2px solid blue';
    document.getElementById('item'+a+'').innerHTML = '';           document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[i][j] = '0';                 
                    
                    
            }else{cv++; //switchd = true;
                 }
            
                
                
            }else{}
       a++;
        }
      //a++;
    
    } 
    }else{}
   
    cv = 0;
    
    
     chec = 0;
     a = 1;
    
    for(var i=1; i<=16; i++){
        document.getElementById('item'+i+'').style.border = '2px solid blue';
    }
    
}

function ConstruirCabaña(item){
  var T = 'Trigo', L = 'Ladrillo', V = 'Vidrio';
  var chec = 0;
  var a = 2;  
     for(var p=1; p<=3; p++){
            
            for(var k=0; k<=2; k++){
             
                
                //==================================patron 1
                if(tablero[p-1][k+1] == T && tablero[p][k] == L && tablero[p][k+1] == V){
                    
                if(item == 'item'+a+''){
                 
                    chec++;
    document.getElementById('item'+a+'').style.border =  '2px solid blue';               
    document.getElementById('item'+a+'').innerHTML = 'Cabaña';       document.getElementById('item'+a+'').style.background = '#751C0E';   tablero[p-1][k+1] = 'Cabaña';
                    
                    
    document.getElementById('item'+(a+3)+'').style.border =  '2px solid blue';               
    document.getElementById('item'+(a+3)+'').innerHTML = '';       document.getElementById('item'+(a+3)+'').style.background = '#F0E68C';   tablero[p][k] = '0';
                    
                    
    document.getElementById('item'+(a+4)+'').style.border =  '2px solid blue';               
    document.getElementById('item'+(a+4)+'').innerHTML = '';       document.getElementById('item'+(a+4)+'').style.background = '#F0E68C';   tablero[p][k+1] = '0';                
                  
                    
                }else if(item == 'item'+(a+3)+''){
    
                    chec++;
    document.getElementById('item'+(a+3)+'').style.border =  '2px solid blue';               
    document.getElementById('item'+(a+3)+'').innerHTML = 'Cabaña';       document.getElementById('item'+(a+3)+'').style.background = '#751C0E';   tablero[p][k] = 'Cabaña';                 
                         
          
    document.getElementById('item'+a+'').style.border =  '2px solid blue';               
    document.getElementById('item'+a+'').innerHTML = '';       document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0';
                    
                    
    document.getElementById('item'+(a+4)+'').style.border =  '2px solid blue';               
    document.getElementById('item'+(a+4)+'').innerHTML = '';       document.getElementById('item'+(a+4)+'').style.background = '#F0E68C';   tablero[p][k+1] = '0';
                    
                    
                }else if(item == 'item'+(a+4)+''){
                
                    chec++;     
     document.getElementById('item'+(a+4)+'').style.border =  '2px solid blue';               
    document.getElementById('item'+(a+4)+'').innerHTML = 'Cabaña';       document.getElementById('item'+(a+4)+'').style.background = '#751C0E';   tablero[p][k+1] = 'Cabaña'; 
                    
                    
    document.getElementById('item'+a+'').style.border =  '2px solid blue';               
    document.getElementById('item'+a+'').innerHTML = '';       document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0'; 
                 
                    
    document.getElementById('item'+(a+3)+'').style.border =  '2px solid blue';               
    document.getElementById('item'+(a+3)+'').innerHTML = '';       document.getElementById('item'+(a+3)+'').style.background = '#F0E68C';   tablero[p][k] = '0';                
                
                }else{}    
                    
                     
                }else{}
    
              a++;  
            }
         a++;
     }
    
    
    //=====================================PATRON 5
    //reiniciamos valor a 
    a = 2;
    
    if(chec == 0){
       
    for(var p=1; p<=3; p++){
            
            for(var k=0; k<=2; k++){    
        
            if(tablero[p-1][k+1] == L && tablero[p][k] == T && tablero[p][k+1] == V){
                
            if(item == 'item'+a+''){    
                
            chec++;
    document.getElementById('item'+a+'').style.border =  '2px solid blue';
    document.getElementById('item'+a+'').innerHTML = 'Cabaña';   document.getElementById('item'+a+'').style.background = '#751C0E';  tablero[p-1][k+1] = 'Cabaña';       
                
                
    document.getElementById('item'+(a+3)+'').style.border =  '2px solid blue';               
    document.getElementById('item'+(a+3)+'').innerHTML = '';       document.getElementById('item'+(a+3)+'').style.background = '#F0E68C';   tablero[p][k] = '0';            
            
    document.getElementById('item'+(a+4)+'').style.border =  '2px solid blue';               
    document.getElementById('item'+(a+4)+'').innerHTML = '';       document.getElementById('item'+(a+4)+'').style.background = '#F0E68C';   tablero[p][k+1] = '0';        
            
            }else if(item == 'item'+(a+3)+''){//---------
                     
     chec++;
    document.getElementById('item'+(a+3)+'').style.border =  '2px solid blue';               
    document.getElementById('item'+(a+3)+'').innerHTML = 'Cabaña';       document.getElementById('item'+(a+3)+'').style.background = '#751C0E';   tablero[p][k] = 'Cabaña'        
                     
     
     document.getElementById('item'+a+'').style.border =  '2px solid blue';               
    document.getElementById('item'+a+'').innerHTML = '';       document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0';
                
    
    document.getElementById('item'+(a+4)+'').style.border =  '2px solid blue';               
    document.getElementById('item'+(a+4)+'').innerHTML = '';       document.getElementById('item'+(a+4)+'').style.background = '#F0E68C';   tablero[p][k+1] = '0';            
            
        }else if(item == 'item'+(a+4)+''){
                 
        chec++;     
     document.getElementById('item'+(a+4)+'').style.border =  '2px solid blue';               
    document.getElementById('item'+(a+4)+'').innerHTML = 'Cabaña';       document.getElementById('item'+(a+4)+'').style.background = '#751C0E';   tablero[p][k+1] = 'Cabaña';    
            
     
    document.getElementById('item'+a+'').style.border =  '2px solid blue';               
    document.getElementById('item'+a+'').innerHTML = '';       document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0';
            
            
    document.getElementById('item'+(a+3)+'').style.border =  '2px solid blue';               
    document.getElementById('item'+(a+3)+'').innerHTML = '';       document.getElementById('item'+(a+3)+'').style.background = '#F0E68C';   tablero[p][k] = '0';         
            }else{}
               
            }else{}
                a++;  
            }
         a++;
     }        
                
    }else{}
    
    //===========================================PATRON 4
   
        //reiniciamos valor a 
    a = 2;
    
    if(chec == 0){
       
    for(var p=1; p<=3; p++){
            
            for(var k=0; k<=2; k++){    
        
            if(tablero[p-1][k] == L && tablero[p-1][k+1] == V && tablero[p][k+1] == T){
                
            if(item == 'item'+a+''){    
                chec++;
    document.getElementById('item'+a+'').style.border =  '2px solid blue';            
    document.getElementById('item'+a+'').innerHTML = 'Cabaña';   document.getElementById('item'+a+'').style.background = '#751C0E';   tablero[p-1][k+1] = 'Cabaña';      
                
    document.getElementById('item'+(a-1)+'').style.border =  '2px solid blue';               
    document.getElementById('item'+(a-1)+'').innerHTML = '';       document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[p-1][k] = '0';            
                
                
    document.getElementById('item'+(a+4)+'').style.border =  '2px solid blue';               
    document.getElementById('item'+(a+4)+'').innerHTML = '';       document.getElementById('item'+(a+4)+'').style.background = '#F0E68C';   tablero[p][k+1] = '0';            
                
            }else if(item == 'item'+(a-1)+''){
            
                chec++;
    document.getElementById('item'+(a-1)+'').style.border =  '2px solid blue';               
    document.getElementById('item'+(a-1)+'').innerHTML = 'Cabaña';       document.getElementById('item'+(a-1)+'').style.background = '#751C0E';   tablero[p-1][k] = 'Cabaña';
                
                
     document.getElementById('item'+a+'').style.border =  '2px solid blue';            
    document.getElementById('item'+a+'').innerHTML = '';   document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0';  
                
                
    document.getElementById('item'+(a+4)+'').style.border =  '2px solid blue';               
    document.getElementById('item'+(a+4)+'').innerHTML = '';       document.getElementById('item'+(a+4)+'').style.background = '#F0E68C';   tablero[p][k+1] = '0'; 
                
                
            }else if(item == 'item'+(a+4)+''){
                     
            chec++;    
    
    document.getElementById('item'+(a+4)+'').style.border =  '2px solid blue';               
    document.getElementById('item'+(a+4)+'').innerHTML = 'Cabaña';       document.getElementById('item'+(a+4)+'').style.background = '#751C0E';   tablero[p][k+1] = 'Cabaña';            
                
      
    document.getElementById('item'+(a-1)+'').style.border =  '2px solid blue';               
    document.getElementById('item'+(a-1)+'').innerHTML = '';       document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[p-1][k] = '0'; 
                
    
    document.getElementById('item'+a+'').style.border =  '2px solid blue';            
    document.getElementById('item'+a+'').innerHTML = '';   document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0';            
                
            }else{}
               
            }else{}
                a++;  
            }
         a++;
     }        
                
    }else{}

    
    
    //============================PATRON 7 
    a = 2;
    
    if(chec == 0){
       
    for(var p=1; p<=3; p++){
            
            for(var k=0; k<=2; k++){    
        
            if(tablero[p-1][k] == T && tablero[p-1][k+1] == V && tablero[p][k+1] == L){
                
            if(item == 'item'+a+''){    
                chec++;
    document.getElementById('item'+a+'').style.border =  '2px solid blue';            
    document.getElementById('item'+a+'').innerHTML = 'Cabaña';   document.getElementById('item'+a+'').style.background = '#751C0E';   tablero[p-1][k+1] = 'Cabaña';      
                
    document.getElementById('item'+(a-1)+'').style.border =  '2px solid blue';               
    document.getElementById('item'+(a-1)+'').innerHTML = '';       document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[p-1][k] = '0';            
                
                
    document.getElementById('item'+(a+4)+'').style.border =  '2px solid blue';               
    document.getElementById('item'+(a+4)+'').innerHTML = '';       document.getElementById('item'+(a+4)+'').style.background = '#F0E68C';   tablero[p][k+1] = '0';            
                
            }else if(item == 'item'+(a-1)+''){
            
                chec++;
    document.getElementById('item'+(a-1)+'').style.border =  '2px solid blue';               
    document.getElementById('item'+(a-1)+'').innerHTML = 'Cabaña';       document.getElementById('item'+(a-1)+'').style.background = '#751C0E';   tablero[p-1][k] = 'Cabaña';
                
                
     document.getElementById('item'+a+'').style.border =  '2px solid blue';            
    document.getElementById('item'+a+'').innerHTML = '';   document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0';  
                
                
    document.getElementById('item'+(a+4)+'').style.border =  '2px solid blue';               
    document.getElementById('item'+(a+4)+'').innerHTML = '';       document.getElementById('item'+(a+4)+'').style.background = '#F0E68C';   tablero[p][k+1] = '0'; 
                
                
            }else if(item == 'item'+(a+4)+''){
                     
            chec++;    
    
    document.getElementById('item'+(a+4)+'').style.border =  '2px solid blue';               
    document.getElementById('item'+(a+4)+'').innerHTML = 'Cabaña';       document.getElementById('item'+(a+4)+'').style.background = '#751C0E';   tablero[p][k+1] = 'Cabaña';            
                
      
    document.getElementById('item'+(a-1)+'').style.border =  '2px solid blue';               
    document.getElementById('item'+(a-1)+'').innerHTML = '';       document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[p-1][k] = '0'; 
                
    
    document.getElementById('item'+a+'').style.border =  '2px solid blue';            
    document.getElementById('item'+a+'').innerHTML = '';   document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0';            
                
            }else{}    
                
                
               
            }else{}
                a++;  
            }
         a++;
     }        
                
    }else{}
    
    
    //===================================PATRON 2
    
    a= 2;
     
    if(chec == 0){
       
    for(var p=1; p<=3; p++){
            
            for(var k=0; k<=2; k++){    
        
            if(tablero[p-1][k] == T && tablero[p][k] == V && tablero[p][k+1] == L){
                
            if(item == 'item'+(a-1)+''){    
                chec++;
document.getElementById('item'+(a-1)+'').style.border =  '2px solid blue';            
document.getElementById('item'+(a-1)+'').innerHTML = 'Cabaña';   document.getElementById('item'+(a-1)+'').style.background = '#751C0E';   tablero[p-1][k] = 'Cabaña';                
//-------------------------------                
                
document.getElementById('item'+(a+3)+'').style.border =  '2px solid blue';
document.getElementById('item'+(a+3)+'').innerHTML = '';       document.getElementById('item'+(a+3)+'').style.background = '#F0E68C';   tablero[p][k] = '0';            
//-------------------------------                
                
document.getElementById('item'+(a+4)+'').style.border =  '2px solid blue';                
document.getElementById('item'+(a+4)+'').innerHTML = '';       document.getElementById('item'+(a+4)+'').style.background = '#F0E68C';   tablero[p][k+1] = '0';                
            
            }else if(item == 'item'+(a+3)+''){
                     chec++;
document.getElementById('item'+(a+3)+'').style.border =  '2px solid blue';
document.getElementById('item'+(a+3)+'').innerHTML = 'Cabaña';       document.getElementById('item'+(a+3)+'').style.background = '#751C0E';   tablero[p][k] = 'Cabaña';                
 //-------------------------------

document.getElementById('item'+(a-1)+'').style.border =  '2px solid blue';            
document.getElementById('item'+(a-1)+'').innerHTML = '';   document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[p-1][k] = '0';                
 //--------------------------------

document.getElementById('item'+(a+4)+'').style.border =  '2px solid blue';                
document.getElementById('item'+(a+4)+'').innerHTML = '';       document.getElementById('item'+(a+4)+'').style.background = '#F0E68C';   tablero[p][k+1] = '0';                
            
            
            }else if(item == 'item'+(a+4)+''){
               chec++;      
                
document.getElementById('item'+(a+4)+'').style.border =  '2px solid blue';                
document.getElementById('item'+(a+4)+'').innerHTML = 'Cabaña';       document.getElementById('item'+(a+4)+'').style.background = '#751C0E';   tablero[p][k+1] = 'Cabaña';                
 //--------------------------------------
                
document.getElementById('item'+(a+3)+'').style.border =  '2px solid blue';
document.getElementById('item'+(a+3)+'').innerHTML = '';       document.getElementById('item'+(a+3)+'').style.background = '#F0E68C';   tablero[p][k] = '0';
                
//-------------------------------------                
document.getElementById('item'+(a-1)+'').style.border =  '2px solid blue';            
document.getElementById('item'+(a-1)+'').innerHTML = '';   document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[p-1][k] = '0';                      
            }else{}
               
            }else{}
                a++;  
            }
         a++;
     }        
                
    }else{}
    
   
//===========================================PATRON 6
a = 2;
    
    if(chec == 0){
       
    for(var p=1; p<=3; p++){
            
            for(var k=0; k<=2; k++){    
        
            if(tablero[p-1][k] == L && tablero[p][k] == V && tablero[p][k+1] == T){
                
            if(item == 'item'+(a-1)+''){    
                chec++;
document.getElementById('item'+(a-1)+'').style.border =  '2px solid blue';            
document.getElementById('item'+(a-1)+'').innerHTML = 'Cabaña';   document.getElementById('item'+(a-1)+'').style.background = '#751C0E';   tablero[p-1][k] = 'Cabaña';                
//-------------------------------                
                
document.getElementById('item'+(a+3)+'').style.border =  '2px solid blue';
document.getElementById('item'+(a+3)+'').innerHTML = '';       document.getElementById('item'+(a+3)+'').style.background = '#F0E68C';   tablero[p][k] = '0';            
//-------------------------------                
                
document.getElementById('item'+(a+4)+'').style.border =  '2px solid blue';                
document.getElementById('item'+(a+4)+'').innerHTML = '';       document.getElementById('item'+(a+4)+'').style.background = '#F0E68C';   tablero[p][k+1] = '0';                
            
            }else if(item == 'item'+(a+3)+''){
                     chec++;
document.getElementById('item'+(a+3)+'').style.border =  '2px solid blue';
document.getElementById('item'+(a+3)+'').innerHTML = 'Cabaña';       document.getElementById('item'+(a+3)+'').style.background = '#751C0E';   tablero[p][k] = 'Cabaña';                
 //-------------------------------

document.getElementById('item'+(a-1)+'').style.border =  '2px solid blue';            
document.getElementById('item'+(a-1)+'').innerHTML = '';   document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[p-1][k] = '0';                
 //--------------------------------

document.getElementById('item'+(a+4)+'').style.border =  '2px solid blue';                
document.getElementById('item'+(a+4)+'').innerHTML = '';       document.getElementById('item'+(a+4)+'').style.background = '#F0E68C';   tablero[p][k+1] = '0';                
            
            
            }else if(item == 'item'+(a+4)+''){
               chec++;      
                
document.getElementById('item'+(a+4)+'').style.border =  '2px solid blue';                
document.getElementById('item'+(a+4)+'').innerHTML = 'Cabaña';       document.getElementById('item'+(a+4)+'').style.background = '#751C0E';   tablero[p][k+1] = 'Cabaña';                
 //--------------------------------------
                
document.getElementById('item'+(a+3)+'').style.border =  '2px solid blue';
document.getElementById('item'+(a+3)+'').innerHTML = '';       document.getElementById('item'+(a+3)+'').style.background = '#F0E68C';   tablero[p][k] = '0';
                
//-------------------------------------                
document.getElementById('item'+(a-1)+'').style.border =  '2px solid blue';            
document.getElementById('item'+(a-1)+'').innerHTML = '';   document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[p-1][k] = '0';                      
            }else{}
               
            }else{}
                a++;  
            }
         a++;
     }        
                
    }else{}    
    
    // ==========================PATRON 3
    
    a = 2;
    
    if(chec == 0){
       
    for(var p=1; p<=3; p++){
            
            for(var k=0; k<=2; k++){    
        
            if(tablero[p-1][k] == V && tablero[p-1][k+1] == L && tablero[p][k] == T){
                
            if(item == 'item'+(a-1)+''){    
                    chec++;
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue'; 
document.getElementById('item'+(a-1)+'').innerHTML = 'Cabaña';   document.getElementById('item'+(a-1)+'').style.background = '#751C0E';   tablero[p-1][k] = 'Cabaña';                
//-------------------------------------
    
document.getElementById('item'+(a)+'').style.border = '2px solid blue';
document.getElementById('item'+(a)+'').innerHTML = '';   document.getElementById('item'+(a)+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0';    
//-------------------------------------
    
document.getElementById('item'+(a+3)+'').style.border = '2px solid blue';   
document.getElementById('item'+(a+3)+'').innerHTML = '';   document.getElementById('item'+(a+3)+'').style.background = '#F0E68C';   tablero[p][k] = '0';                
                
            }else if(item == 'item'+(a)+''){
               chec++;      

document.getElementById('item'+(a)+'').style.border = '2px solid blue';
document.getElementById('item'+(a)+'').innerHTML = 'Cabaña';   document.getElementById('item'+(a)+'').style.background = '#751C0E';   tablero[p-1][k+1] = 'Cabaña';                
                
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue'; 
document.getElementById('item'+(a-1)+'').innerHTML = '';   document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[p-1][k] = '0'; 

document.getElementById('item'+(a+3)+'').style.border = '2px solid blue';   
document.getElementById('item'+(a+3)+'').innerHTML = '';   document.getElementById('item'+(a+3)+'').style.background = '#F0E68C';   tablero[p][k] = '0';                
                
            }else if(item == 'item'+(a+3)+''){
               chec++;      
    
document.getElementById('item'+(a+3)+'').style.border = '2px solid blue';   
document.getElementById('item'+(a+3)+'').innerHTML = 'Cabaña';   document.getElementById('item'+(a+3)+'').style.background = '#751C0E';   tablero[p][k] = 'Cabaña';                
                
document.getElementById('item'+(a)+'').style.border = '2px solid blue';
document.getElementById('item'+(a)+'').innerHTML = '';   document.getElementById('item'+(a)+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0';                     
            
            
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue'; 
document.getElementById('item'+(a-1)+'').innerHTML = '';   document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[p-1][k] = '0';                
            
            }else{}
               
            }else{}
                a++;  
            }
         a++;
     }        
                
    }else{}
    
    
    //===============================PATRON 8
    a = 2;
    
        if(chec == 0){
       
    for(var p=1; p<=3; p++){
            
            for(var k=0; k<=2; k++){    
        
            if(tablero[p-1][k] == V && tablero[p-1][k+1] == T && tablero[p][k] == L){
                
            if(item == 'item'+(a-1)+''){    
                    chec++;
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue'; 
document.getElementById('item'+(a-1)+'').innerHTML = 'Cabaña';   document.getElementById('item'+(a-1)+'').style.background = '#751C0E';   tablero[p-1][k] = 'Cabaña';                
//-------------------------------------
    
document.getElementById('item'+(a)+'').style.border = '2px solid blue';
document.getElementById('item'+(a)+'').innerHTML = '';   document.getElementById('item'+(a)+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0';    
//-------------------------------------
    
document.getElementById('item'+(a+3)+'').style.border = '2px solid blue';   
document.getElementById('item'+(a+3)+'').innerHTML = '';   document.getElementById('item'+(a+3)+'').style.background = '#F0E68C';   tablero[p][k] = '0';                
                
            }else if(item == 'item'+(a)+''){
               chec++;      

document.getElementById('item'+(a)+'').style.border = '2px solid blue';
document.getElementById('item'+(a)+'').innerHTML = 'Cabaña';   document.getElementById('item'+(a)+'').style.background = '#751C0E';   tablero[p-1][k+1] = 'Cabaña';                
                
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue'; 
document.getElementById('item'+(a-1)+'').innerHTML = '';   document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[p-1][k] = '0'; 

document.getElementById('item'+(a+3)+'').style.border = '2px solid blue';   
document.getElementById('item'+(a+3)+'').innerHTML = '';   document.getElementById('item'+(a+3)+'').style.background = '#F0E68C';   tablero[p][k] = '0';                
                
            }else if(item == 'item'+(a+3)+''){
               chec++;      
    
document.getElementById('item'+(a+3)+'').style.border = '2px solid blue';   
document.getElementById('item'+(a+3)+'').innerHTML = 'Cabaña';   document.getElementById('item'+(a+3)+'').style.background = '#751C0E';   tablero[p][k] = 'Cabaña';                
                
document.getElementById('item'+(a)+'').style.border = '2px solid blue';
document.getElementById('item'+(a)+'').innerHTML = '';   document.getElementById('item'+(a)+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0';                     
            
            
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue'; 
document.getElementById('item'+(a-1)+'').innerHTML = '';   document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[p-1][k] = '0';                
            
            }else{}
               
            }else{}
                a++;  
            }
         a++;
     }        
                
    }else{}
    
    a = 2;
    
        for(var i=1; i<=16; i++){
        document.getElementById('item'+i+'').style.border = '2px solid blue';
    }
    
}

function ConstruirGranja(item){

    var T = 'Trigo', M = 'Madera';   
   var chec = 0;
    var a = 2;
    
    for(var p=1; p<=3; p++){
            
            for(var k=0; k<=2; k++){
            
                //PATRON 1,2,3,4
            if((tablero[p-1][k] == T && tablero[p-1][k+1] == T && tablero[p][k] == M && tablero[p][k+1] == M) || (tablero[p-1][k] == T && tablero[p-1][k+1] == M && tablero[p][k] == T && tablero[p][k+1] == M) || (tablero[p-1][k] == M && tablero[p-1][k+1] == M && tablero[p][k] == T && tablero[p][k+1] == T) || (tablero[p-1][k] == M && tablero[p-1][k+1] == T && tablero[p][k] == M && tablero[p][k+1] == T)){
    
            if(item == 'item'+a+''){
            chec++;
 document.getElementById('item'+a+'').style.border = '2px solid blue';
 document.getElementById('item'+a+'').innerHTML = 'Granja';       document.getElementById('item'+a+'').style.background = '#751C0E';   tablero[p-1][k+1] = 'Granja';
                
                
 document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
 document.getElementById('item'+(a-1)+'').innerHTML = '';       document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[p-1][k] = '0';
                
                
 document.getElementById('item'+(a+3)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+3)+'').innerHTML = '';       document.getElementById('item'+(a+3)+'').style.background = '#F0E68C';   tablero[p][k] = '0';                
                
                
 document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';                  
document.getElementById('item'+(a+4)+'').innerHTML = '';       document.getElementById('item'+(a+4)+'').style.background = '#F0E68C';   tablero[p][k+1] = '0';                
                
            }else if(item == 'item'+(a-1)+''){
                chec++;
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
 document.getElementById('item'+(a-1)+'').innerHTML = 'Granja';       document.getElementById('item'+(a-1)+'').style.background = '#751C0E';   tablero[p-1][k] = 'Granja';
                
                
document.getElementById('item'+a+'').style.border = '2px solid blue';
 document.getElementById('item'+a+'').innerHTML = '';       document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0'; 
                

document.getElementById('item'+(a+3)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+3)+'').innerHTML = '';       document.getElementById('item'+(a+3)+'').style.background = '#F0E68C';   tablero[p][k] = '0';
                
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';                  
document.getElementById('item'+(a+4)+'').innerHTML = '';       document.getElementById('item'+(a+4)+'').style.background = '#F0E68C';   tablero[p][k+1] = '0';                 
                     
            }else if(item == 'item'+(a+3)+''){
                  chec++;   
document.getElementById('item'+(a+3)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+3)+'').innerHTML = 'Granja';       document.getElementById('item'+(a+3)+'').style.background = '#751C0E';   tablero[p][k] = 'Granja';
                
                
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
 document.getElementById('item'+(a-1)+'').innerHTML = '';       document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[p-1][k] = '0';
                
document.getElementById('item'+a+'').style.border = '2px solid blue';
 document.getElementById('item'+a+'').innerHTML = '';       document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0';                 
                

document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';                  
document.getElementById('item'+(a+4)+'').innerHTML = '';       document.getElementById('item'+(a+4)+'').style.background = '#F0E68C';   tablero[p][k+1] = '0';                
            
            }else if(item == 'item'+(a+4)+''){
                chec++;   
                
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';                  
document.getElementById('item'+(a+4)+'').innerHTML = 'Granja';       document.getElementById('item'+(a+4)+'').style.background = '#751C0E';   tablero[p][k+1] = 'Granja';
                
document.getElementById('item'+a+'').style.border = '2px solid blue';
 document.getElementById('item'+a+'').innerHTML = '';       document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0';                
                
                
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
 document.getElementById('item'+(a-1)+'').innerHTML = '';       document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[p-1][k] = '0';   
                
                
document.getElementById('item'+(a+3)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+3)+'').innerHTML = '';       document.getElementById('item'+(a+3)+'').style.background = '#F0E68C';   tablero[p][k] = '0';                
                     
            }else{}    
    
            }else{}
                
                a++;
            }
                a++;
    }
    
    a = 2;
    
        for(var i=1; i<=16; i++){
        document.getElementById('item'+i+'').style.border = '2px solid blue';
    }
    
}

function ConstruirAsilo(item){
    
var V = 'Vidrio', P = 'Piedra';
var a = 3;    
var chec = 0;    
    
        for(var i=0; i<=3; i++){
        
        for(var j=0; j<=1; j++){
            
    //PATRON 1
            if(tablero[i][j] == P && tablero[i][j+1] == P && tablero[i][j+2] == V){        
         
                if(item == 'item'+a+''){
                    chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = 'Asilo';         document.getElementById('item'+a+'').style.background = '#751C0E';   tablero[i][j+2] = 'Asilo';
                

document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';         document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[i][j+1] = '0';
                

document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';         document.getElementById('item'+(a-2)+'').style.background = '#F0E68C';   tablero[i][j] = '0';                
                
                }else if(item == 'item'+(a-1)+''){
                         chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';         document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[i][j+2] = '';
                

document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = 'Asilo';         document.getElementById('item'+(a-1)+'').style.background = '#751C0E';   tablero[i][j+1] = 'Asilo';
                

document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';         document.getElementById('item'+(a-2)+'').style.background = '#F0E68C';   tablero[i][j] = '0';                         
                         
                }else if(item == 'item'+(a-2)+''){
                         chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';         document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[i][j+2] = '0';
                

document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';         document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[i][j+1] = '0';
                

document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = 'Asilo';         document.getElementById('item'+(a-2)+'').style.background = '#751C0E';   tablero[i][j] = 'Asilo';                         
                         
                }else{}
             
               
            }else{}
            a++;
        }
            a+=2;
        }
   
    //================================PATRON 2
a = 3;    
    if(chec == 0){
       
      for(var i=0; i<=3; i++){
        
        for(var j=0; j<=1; j++){
        
        if(tablero[i][j] == V && tablero[i][j+1] == P && tablero[i][j+2] == P){    
            
           if(item == 'item'+a+''){
                    chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = 'Asilo';         document.getElementById('item'+a+'').style.background = '#751C0E';   tablero[i][j+2] = 'Asilo';
                

document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';         document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[i][j+1] = '0';
                

document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';         document.getElementById('item'+(a-2)+'').style.background = '#F0E68C';   tablero[i][j] = '0';                
                
                }else if(item == 'item'+(a-1)+''){
                         chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';         document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[i][j+2] = '';
                

document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = 'Asilo';         document.getElementById('item'+(a-1)+'').style.background = '#751C0E';   tablero[i][j+1] = 'Asilo';
                

document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';         document.getElementById('item'+(a-2)+'').style.background = '#F0E68C';   tablero[i][j] = '0';                         
                         
                }else if(item == 'item'+(a-2)+''){
                         chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';         document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[i][j+2] = '0';
                

document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';         document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[i][j+1] = '0';
                

document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = 'Asilo';         document.getElementById('item'+(a-2)+'').style.background = '#751C0E';   tablero[i][j] = 'Asilo';                         
                         
                }else{}
            
        }else{}
            a++;
        }
         a+=2; 
      }
            
     }else{}    
    
    //==========================PATRON 3
    a =1;

    if(chec == 0){
        
        for(var i=0; i<=1; i++){
        
        for(var j=0; j<=3; j++){
        
            if(tablero[i][j] == V && tablero[i+1][j] == P && tablero[i+2][j] == P){
                
            if(item == 'item'+a+''){
               
               chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = 'Asilo';         document.getElementById('item'+a+'').style.background = '#751C0E';   tablero[i][j] = 'Asilo';
                
                
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';         document.getElementById('item'+(a+4)+'').style.background = '#F0E68C';   tablero[i+1][j] = '0';
                

document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';         document.getElementById('item'+(a+8)+'').style.background = '#F0E68C';   tablero[i+2][j] = '0';                
                
                
            }else if(item == 'item'+(a+4)+''){
             chec++;        
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';         document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[i][j] = '0';
                
                
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = 'Asilo';         document.getElementById('item'+(a+4)+'').style.background = '#751C0E';   tablero[i+1][j] = 'Asilo';
                

document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';         document.getElementById('item'+(a+8)+'').style.background = '#F0E68C';   tablero[i+2][j] = '0';             
                
                     
            }else if(item == 'item'+(a+8)+''){
             chec++;        
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';         document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[i][j] = '0';
                
                
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';         document.getElementById('item'+(a+4)+'').style.background = '#F0E68C';   tablero[i+1][j] = '0';
                

document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = 'Asilo';         document.getElementById('item'+(a+8)+'').style.background = '#751C0E';   tablero[i+2][j] = 'Asilo';             
                
                     
            }else{}
                
            
            }else{}
            
            a++;
        }
            
        }
        
    }else{}
    //======================================PATRON 4
    a = 1;
    if(chec == 0){
    
    for(var i=0; i<=1; i++){
        
        for(var j=0; j<=3; j++){
        
            if(tablero[i][j] == P && tablero[i+1][j] == P && tablero[i+2][j] == V){
                
            if(item == 'item'+a+''){
               
               chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = 'Asilo';         document.getElementById('item'+a+'').style.background = '#48A6C4';   tablero[i][j] = 'Asilo';
                
                
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';         document.getElementById('item'+(a+4)+'').style.background = '#F0E68C';   tablero[i+1][j] = '0';
                

document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';         document.getElementById('item'+(a+8)+'').style.background = '#F0E68C';   tablero[i+2][j] = '0';                
                
                
            }else if(item == 'item'+(a+4)+''){
             chec++;        
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';         document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[i][j] = '0';
                
                
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = 'Asilo';         document.getElementById('item'+(a+4)+'').style.background = '#751C0E';   tablero[i+1][j] = 'Asilo';
                

document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';         document.getElementById('item'+(a+8)+'').style.background = '#F0E68C';   tablero[i+2][j] = '0';             
                
                     
            }else if(item == 'item'+(a+8)+''){
             chec++;        
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';         document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[i][j] = '0';
                
                
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';         document.getElementById('item'+(a+4)+'').style.background = '#F0E68C';   tablero[i+1][j] = '0';
                

document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = 'Asilo';         document.getElementById('item'+(a+8)+'').style.background = '#751C0E';   tablero[i+2][j] = 'Asilo';             
                
                     
            }else{}
                
            
            }else{}
            
            a++;
        }
            
        }    
        
       
    }else{}
    
    a = 3;
    
        for(var i=1; i<=16; i++){
        document.getElementById('item'+i+'').style.border = '2px solid blue';
    }
    
}

function ConstruirCapilla(item){
     var V = 'Vidrio', P = 'Piedra';
     var chec = 0;
     var a = 3;
     var b = 5;
    
    //patron 3
    for(var p=1; p<=3; p++){
        
        for(var k=0; k<=1; k++){
    
        if(tablero[p-1][k] == P && tablero[p-1][k+1] == V && tablero[p-1][k+2] == P && tablero[p][k] == V){    
            
        if(item == 'item'+a+''){
           chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = 'Capilla';         document.getElementById('item'+a+'').style.background = '#751C0E';   tablero[p-1][k+2] = 'Capilla';
            
            
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';         document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0';            
        
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';         document.getElementById('item'+(a-2)+'').style.background = '#F0E68C';   tablero[p-1][k] = '0';            
        
document.getElementById('item'+b+'').style.border = '2px solid blue'; document.getElementById('item'+b+'').innerHTML = '';         document.getElementById('item'+b+'').style.background = '#F0E68C';   tablero[p][k] = '0';   
           
           
        }else if(item == 'item'+(a-1)+''){
           chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';         document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[p-1][k+2] = '0';
            
            
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = 'Capilla';         document.getElementById('item'+(a-1)+'').style.background = '#751C0E';   tablero[p-1][k+1] = 'Capilla';            
        
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';         document.getElementById('item'+(a-2)+'').style.background = '#F0E68C';   tablero[p-1][k] = '0';            
        
document.getElementById('item'+b+'').style.border = '2px solid blue'; document.getElementById('item'+b+'').innerHTML = '';         document.getElementById('item'+b+'').style.background = '#F0E68C';   tablero[p][k] = '0';   
           
           
        }else if(item == 'item'+(a-2)+''){
           chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';         document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[p-1][k+2] = '0';
            
            
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';         document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0';            
        
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = 'Capilla';         document.getElementById('item'+(a-2)+'').style.background = '#751C0E';   tablero[p-1][k] = 'Capilla';            
        
document.getElementById('item'+b+'').style.border = '2px solid blue'; document.getElementById('item'+b+'').innerHTML = '';         document.getElementById('item'+b+'').style.background = '#F0E68C';   tablero[p][k] = '0';   
           
           
        }else if(item == 'item'+b+''){
           chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';         document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[p-1][k+2] = '0';
            
            
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';         document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0';            
        
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';         document.getElementById('item'+(a-2)+'').style.background = '#F0E68C';   tablero[p-1][k] = '0';            
        
document.getElementById('item'+b+'').style.border = '2px solid blue'; document.getElementById('item'+b+'').innerHTML = 'Capilla';         document.getElementById('item'+b+'').style.background = '#751C0E';   tablero[p][k] = 'Capilla';   
           
           
        }else{}
            
        }else{}
            a++; b++;
        }
            a+=2; b+=2;
    }
    
    
    //=============== patron 4
    a=3; b=7;
    
    if(chec == 0){
     for(var p=1; p<=3; p++){
        
        for(var k=0; k<=1; k++){   
        
        if(tablero[p-1][k] == P && tablero[p-1][k+1] == V && tablero[p-1][k+2] == P && tablero[p][k+2] == V){    
            
            if(item == 'item'+a+''){
                chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = 'Capilla';         document.getElementById('item'+a+'').style.background = '#751C0E';   tablero[p-1][k+2] = 'Capilla';
            
            
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';         document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0';            
        
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';         document.getElementById('item'+(a-2)+'').style.background = '#F0E68C';   tablero[p-1][k] = '0';            
        
document.getElementById('item'+b+'').style.border = '2px solid blue'; document.getElementById('item'+b+'').innerHTML = '';         document.getElementById('item'+b+'').style.background = '#F0E68C';   tablero[p][k+2] = '0';
            
            }else if(item == 'item'+(a-1)+''){
           chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';         document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[p-1][k+2] = '0';
            
            
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = 'Capilla';         document.getElementById('item'+(a-1)+'').style.background = '#751C0E';   tablero[p-1][k+1] = 'Capilla';            
        
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';         document.getElementById('item'+(a-2)+'').style.background = '#F0E68C';   tablero[p-1][k] = '0';            
        
document.getElementById('item'+b+'').style.border = '2px solid blue'; document.getElementById('item'+b+'').innerHTML = '';         document.getElementById('item'+b+'').style.background = '#F0E68C';   tablero[p][k+2] = '0';   
           
           
        }else if(item == 'item'+(a-2)+''){
           chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';         document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[p-1][k+2] = '0';
            
            
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';         document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0';            
        
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = 'Capilla';         document.getElementById('item'+(a-2)+'').style.background = '#751C0E';   tablero[p-1][k] = 'Capilla';            
        
document.getElementById('item'+b+'').style.border = '2px solid blue'; document.getElementById('item'+b+'').innerHTML = '';         document.getElementById('item'+b+'').style.background = '#F0E68C';   tablero[p][k+2] = '0';   
           
           
        }else if(item == 'item'+b+''){
           chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';         document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[p-1][k+2] = '0';
            
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';         document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0';            
        
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';         document.getElementById('item'+(a-2)+'').style.background = '#F0E68C';   tablero[p-1][k] = '0';            
        
document.getElementById('item'+b+'').style.border = '2px solid blue'; document.getElementById('item'+b+'').innerHTML = 'Capilla';         document.getElementById('item'+b+'').style.background = '#751C0E';   tablero[p][k+2] = 'Capilla';   
           
           
        }else{}
    
        }else{}
            a++; b++;
        }
         a+=2; b+=2;
     }
    }else{}
 
    //==================PATRON 1
    a=7; b=3;
    
    if(chec == 0){
     for(var p=1; p<=3; p++){
        
        for(var k=0; k<=1; k++){   
        
        if(tablero[p-1][k+2] == V && tablero[p][k] == P && tablero[p][k+1] == V && tablero[p][k+2] == P){    
            
            if(item == 'item'+a+''){
                chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = 'Capilla';         document.getElementById('item'+a+'').style.background = '#751C0E';   tablero[p][k+2] = 'Capilla';                
                
                
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';         document.getElementById('item'+(a-1)+'').style.background = '#F0E68C'; tablero[p][k+1] = '0';                
                
                
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';         document.getElementById('item'+(a-2)+'').style.background = '#F0E68C'; tablero[p][k] = '0';                 
                
                
document.getElementById('item'+b+'').style.border = '2px solid blue';   
document.getElementById('item'+b+'').innerHTML = '';         document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[p-1][k+2] = '0';                
                
                
            }else if(item == 'item'+(a-1)+''){
                chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';         document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[p][k+2] = '0';                
                
                
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = 'Capilla';         document.getElementById('item'+(a-1)+'').style.background = '#751C0E'; tablero[p][k+1] = 'Capilla';                
                
                
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';         document.getElementById('item'+(a-2)+'').style.background = '#F0E68C'; tablero[p][k] = '0';                 
                
                
document.getElementById('item'+b+'').style.border = '2px solid blue';   
document.getElementById('item'+b+'').innerHTML = '';         document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[p-1][k+2] = '0';                
                
                
            }else if(item == 'item'+(a-2)+''){
                chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';         document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[p][k+2] = '0';                
                
                
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';         document.getElementById('item'+(a-1)+'').style.background = '#F0E68C'; tablero[p][k+1] = '0';                
                
                
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = 'Capilla';         document.getElementById('item'+(a-2)+'').style.background = '#751C0E'; tablero[p][k] = 'Capilla';                 
                
                
document.getElementById('item'+b+'').style.border = '2px solid blue';   
document.getElementById('item'+b+'').innerHTML = '';         document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[p-1][k+2] = '0';                
                
                
            }else if(item == 'item'+b+''){
                chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';         document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[p][k+2] = '0';                
                
                
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';         document.getElementById('item'+(a-1)+'').style.background = '#F0E68C'; tablero[p][k+1] = '0';                
                
                
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';         document.getElementById('item'+(a-2)+'').style.background = '#F0E68C'; tablero[p][k] = '0';                 
                
                
document.getElementById('item'+b+'').style.border = '2px solid blue';   
document.getElementById('item'+b+'').innerHTML = 'Capilla';         document.getElementById('item'+b+'').style.background = '#751C0E'; tablero[p-1][k+2] = 'Capilla';                
                
                
            }else{}
    
        }else{}
            a++; b++;
        }
         a+=2; b+=2;
     }
    
    }else{}
    
    
    //==================PATRON 2
    a=7; b=1;
    
    if(chec == 0){
     for(var p=1; p<=3; p++){
        
        for(var k=0; k<=1; k++){   
        
        if(tablero[p-1][k] == V && tablero[p][k] == P && tablero[p][k+1] == V && tablero[p][k+2] == P){    
            
            if(item == 'item'+a+''){
                chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = 'Capilla';         document.getElementById('item'+a+'').style.background = '#751C0E';   tablero[p][k+2] = 'Capilla';                
                
                
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';         document.getElementById('item'+(a-1)+'').style.background = '#F0E68C'; tablero[p][k+1] = '0';                
                
                
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';         document.getElementById('item'+(a-2)+'').style.background = '#F0E68C'; tablero[p][k] = '0';                 
                
                
document.getElementById('item'+b+'').style.border = '2px solid blue';   
document.getElementById('item'+b+'').innerHTML = '';         document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[p-1][k] = '0';                
                
                
            }else if(item == 'item'+(a-1)+''){
                chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';         document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[p][k+2] = '0';                
                
                
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = 'Capilla';         document.getElementById('item'+(a-1)+'').style.background = '#751C0E'; tablero[p][k+1] = 'Capilla';                
                
                
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';         document.getElementById('item'+(a-2)+'').style.background = '#F0E68C'; tablero[p][k] = '0';                 
                
                
document.getElementById('item'+b+'').style.border = '2px solid blue';   
document.getElementById('item'+b+'').innerHTML = '';         document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[p-1][k] = '0';                
                
                
            }else if(item == 'item'+(a-2)+''){
                chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';         document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[p][k+2] = '0';                
                
                
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';         document.getElementById('item'+(a-1)+'').style.background = '#F0E68C'; tablero[p][k+1] = '0';                
                
                
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = 'Capilla';         document.getElementById('item'+(a-2)+'').style.background = '#751C0E'; tablero[p][k] = 'Capilla';                 
                
                
document.getElementById('item'+b+'').style.border = '2px solid blue';   
document.getElementById('item'+b+'').innerHTML = '';         document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[p-1][k] = '0';                
                
                
            }else if(item == 'item'+b+''){
                chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';         document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[p][k+2] = '0';                
                
                
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';         document.getElementById('item'+(a-1)+'').style.background = '#F0E68C'; tablero[p][k+1] = '0';                
                
                
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';         document.getElementById('item'+(a-2)+'').style.background = '#F0E68C'; tablero[p][k] = '0';                 
                
                
document.getElementById('item'+b+'').style.border = '2px solid blue';   
document.getElementById('item'+b+'').innerHTML = 'Capilla';         document.getElementById('item'+b+'').style.background = '#751C0E'; tablero[p-1][k] = 'Capilla';                
                
                
            }else{}
    
        }else{}
            a++; b++;
        }
         a+=2; b+=2;
     }
    
    }else{}
    
    ///====================PATRON 5
    a=2; b=9;
    
    if(chec == 0){
     for(var i=0; i<=1; i++){
        
        for(var l=1; l<=3; l++){    
        
        if(tablero[i][l] == P && tablero[i+1][l] == V && tablero[i+2][l] == P && tablero[i+2][l-1] == V){    
            
            if(item == 'item'+a+''){
                chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = 'Capilla';     document.getElementById('item'+a+'').style.background = '#751C0E';   tablero[i][l] = 'Capilla';                

document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';     document.getElementById('item'+(a+4)+'').style.background = '#F0E68C';   tablero[i+1][l] = '0';                

document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';     document.getElementById('item'+(a+8)+'').style.background = '#F0E68C';   tablero[i+2][l] = '0';                

document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';     document.getElementById('item'+b+'').style.background = '#F0E68C';   tablero[i+2][l-1] = '0';                
                
                
            }else if(item == 'item'+(a+4)+''){
                chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';     document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[i][l] = '0';                

document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = 'Capilla';     document.getElementById('item'+(a+4)+'').style.background = '#751C0E';   tablero[i+1][l] = 'Capilla';                

document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';     document.getElementById('item'+(a+8)+'').style.background = '#F0E68C';   tablero[i+2][l] = '0';                

document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';     document.getElementById('item'+b+'').style.background = '#F0E68C';   tablero[i+2][l-1] = '0';                
                
                
            }else if(item == 'item'+(a+8)+''){
                chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';     document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[i][l] = '0';                

document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';     document.getElementById('item'+(a+4)+'').style.background = '#F0E68C';   tablero[i+1][l] = '0';                

document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = 'Capilla';     document.getElementById('item'+(a+8)+'').style.background = '#751C0E';   tablero[i+2][l] = 'Capilla';                

document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';     document.getElementById('item'+b+'').style.background = '#F0E68C';   tablero[i+2][l-1] = '0';                
                
                
            }else if(item == 'item'+b+''){
                chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';     document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[i][l] = '0';                

document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';     document.getElementById('item'+(a+4)+'').style.background = '#F0E68C';   tablero[i+1][l] = '0';                

document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';     document.getElementById('item'+(a+8)+'').style.background = '#F0E68C';   tablero[i+2][l] = '0';                

document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = 'Capilla';     document.getElementById('item'+b+'').style.background = '#751C0E';   tablero[i+2][l-1] = 'Capilla';                
                
                
            }else{}
    
        }else{}
            a++; b++;
        }
            a++; b++;
     }
    }else{}
    
    //===================PATRON 7
        a = 2;
        b = 1;    
        if(chec == 0){    
        for(var i=0; i<=1; i++){
        
        for(var l=1; l<=3; l++){    
            //patron 7    
        
        if(tablero[i][l] == P && tablero[i+1][l] == V && tablero[i+2][l] == P && tablero[i][l-1] == V){    
         
            if(item == 'item'+a+''){
            chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = 'Capilla';     document.getElementById('item'+a+'').style.background = '#751C0E';   tablero[i][l] = 'Capilla';
            
            
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';     document.getElementById('item'+(a+4)+'').style.background = '#F0E68C';   tablero[i+1][l] = '0';            
        
            
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';     document.getElementById('item'+(a+8)+'').style.background = '#F0E68C';   tablero[i+2][l] = '0';
        
            
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = ''; document.getElementById('item'+b+'').style.background = '#F0E68C';   tablero[i][l-1] = '0';
            
            }else if(item == 'item'+(a+4)+''){
            chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';     document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[i][l] = '0';
            
            
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = 'Capilla';     document.getElementById('item'+(a+4)+'').style.background = '#751C0E';   tablero[i+1][l] = 'Capilla';            
        
            
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';     document.getElementById('item'+(a+8)+'').style.background = '#48A6C4';   tablero[i+2][l] = '0';
        
            
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = ''; document.getElementById('item'+b+'').style.background = '#F0E68C';   tablero[i][l-1] = '0';
            
            }else if(item == 'item'+(a+8)+''){
            chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';     document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[i][l] = '0';
            
            
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';     document.getElementById('item'+(a+4)+'').style.background = '#F0E68C';   tablero[i+1][l] = '0';            
        
            
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = 'Capilla';     document.getElementById('item'+(a+8)+'').style.background = '#751C0E';   tablero[i+2][l] = 'Capilla';
        
            
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = ''; document.getElementById('item'+b+'').style.background = '#F0E68C';   tablero[i][l-1] = '0';
            
            }else if(item == 'item'+b+''){
            chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';     document.getElementById('item'+a+'').style.background = '#F0E68C';   tablero[i][l] = '0';
            
            
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';     document.getElementById('item'+(a+4)+'').style.background = '#F0E68C';   tablero[i+1][l] = '0';            
        
            
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';     document.getElementById('item'+(a+8)+'').style.background = '#F0E68C';   tablero[i+2][l] = '0';
        
            
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = 'Capilla'; document.getElementById('item'+b+'').style.background = '#751C0E';   tablero[i][l-1] = 'Capilla';
            }else{} 
            
        }else{}
         a++;
            b++;
        }
           a++;
            b++;
        }
            
        }else{}
    
    //==================PATRON 6
    a = 1;
    b = 10;
    if(chec == 0){
        
    for(var i=0; i<=1; i++){
        
        for(var l=1; l<=3; l++){    
        
        if(tablero[i][l-1] == P && tablero[i+1][l-1] == V && tablero[i+2][l-1] == P && tablero[i+2][l] == V){    
            
            if(item == 'item'+a+''){
            chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue'; 
document.getElementById('item'+a+'').innerHTML = 'Capilla';         document.getElementById('item'+a+'').style.background = '#751C0E'; tablero[i][l-1] = 'Capilla';      
                
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';         document.getElementById('item'+(a+4)+'').style.background = '#F0E68C'; tablero[i+1][l-1] = '0';
    
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';         document.getElementById('item'+(a+8)+'').style.background = '#F0E68C'; tablero[i+2][l-1] = '0';                
                
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';         document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[i+2][l] = '0';                
        
            }else if(item == 'item'+(a+4)+''){
            chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue'; 
document.getElementById('item'+a+'').innerHTML = '';         document.getElementById('item'+a+'').style.background = '#F0E68C'; tablero[i][l-1] = '0';      
                
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = 'Capilla';         document.getElementById('item'+(a+4)+'').style.background = '#751C0E'; tablero[i+1][l-1] = 'Capilla';
    
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';         document.getElementById('item'+(a+8)+'').style.background = '#F0E68C'; tablero[i+2][l-1] = '0';                
                
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';         document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[i+2][l] = '0';            
                 
                 
        }else if(item == 'item'+(a+8)+''){
            chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue'; 
document.getElementById('item'+a+'').innerHTML = '';         document.getElementById('item'+a+'').style.background = '#F0E68C'; tablero[i][l-1] = '0';      
                
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';         document.getElementById('item'+(a+4)+'').style.background = '#F0E68C'; tablero[i+1][l-1] = '0';
    
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = 'Capilla';         document.getElementById('item'+(a+8)+'').style.background = '#751C0E'; tablero[i+2][l-1] = 'Capilla';                
                
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';         document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[i+2][l] = '0';            
                 
                 
        }else if(item == 'item'+b+''){
            chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue'; 
document.getElementById('item'+a+'').innerHTML = '';         document.getElementById('item'+a+'').style.background = '#751C0E'; tablero[i][l-1] = '0';      
                
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';         document.getElementById('item'+(a+4)+'').style.background = '#751C0E'; tablero[i+1][l-1] = '0';
    
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';         document.getElementById('item'+(a+8)+'').style.background = '#751C0E'; tablero[i+2][l-1] = '0';                
                
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = 'Capilla';         document.getElementById('item'+b+'').style.background = '#751C0E'; tablero[i+2][l] = 'Capilla';            
        }else{}
                
        
            
        }else{}  
            
           a++; b++; 
        }
            a++; b++;
    }
        
    }else{}
    
//===================================PATRON 8

 a = 1;
    b = 2;
    if(chec == 0){
        
    for(var i=0; i<=1; i++){
        
        for(var l=1; l<=3; l++){    
        
        if(tablero[i][l-1] == P && tablero[i+1][l-1] == V && tablero[i+2][l-1] == P && tablero[i][l] == V){    
            
            if(item == 'item'+a+''){
            chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue'; 
document.getElementById('item'+a+'').innerHTML = 'Capilla';         document.getElementById('item'+a+'').style.background = '#751C0E'; tablero[i][l-1] = 'Capilla';      
                
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';         document.getElementById('item'+(a+4)+'').style.background = '#F0E68C'; tablero[i+1][l-1] = '0';
    
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';         document.getElementById('item'+(a+8)+'').style.background = '#F0E68C'; tablero[i+2][l-1] = '0';                
                
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';         document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[i][l] = '0';                
        
            }else if(item == 'item'+(a+4)+''){
            chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue'; 
document.getElementById('item'+a+'').innerHTML = '';         document.getElementById('item'+a+'').style.background = '#F0E68C'; tablero[i][l-1] = '0';      
                
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = 'Capilla';         document.getElementById('item'+(a+4)+'').style.background = '#751C0E'; tablero[i+1][l-1] = 'Capilla';
    
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';         document.getElementById('item'+(a+8)+'').style.background = '#F0E68C'; tablero[i+2][l-1] = '0';                
                
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';         document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[i][l] = '0';            
                 
                 
        }else if(item == 'item'+(a+8)+''){
            chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue'; 
document.getElementById('item'+a+'').innerHTML = '';         document.getElementById('item'+a+'').style.background = '#F0E68C'; tablero[i][l-1] = '0';      
                
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';         document.getElementById('item'+(a+4)+'').style.background = '#F0E68C'; tablero[i+1][l-1] = '0';
    
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = 'Capilla';         document.getElementById('item'+(a+8)+'').style.background = '#751C0E'; tablero[i+2][l-1] = 'Capilla';                
                
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';         document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[i][l] = '0';            
                 
                 
        }else if(item == 'item'+b+''){
            chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue'; 
document.getElementById('item'+a+'').innerHTML = '';         document.getElementById('item'+a+'').style.background = '#F0E68C'; tablero[i][l-1] = '0';      
                
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';         document.getElementById('item'+(a+4)+'').style.background = '#F0E68C'; tablero[i+1][l-1] = '0';
    
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';         document.getElementById('item'+(a+8)+'').style.background = '#F0E68C'; tablero[i+2][l-1] = '0';                
                
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = 'Capilla';         document.getElementById('item'+b+'').style.background = '#751C0E'; tablero[i][l] = 'Capilla';            
        }else{}
                
        
            
        }else{}  
            
           a++; b++; 
        }
            a++; b++;
    }
        
    }else{}    
    
        for(var i=1; i<=16; i++){
        document.getElementById('item'+i+'').style.border = '2px solid blue';
    }
    
}

function ConstruirFabrica(item){
    var M = 'Madera', L='Ladrillo', P='Piedra';
    var chec = 0;
    var a = 8, b = 1;
    
    //==============PATRON 1
    for(var p=1; p<=3; p++){
    var z=0;
        
    if(tablero[p-1][z] == M && tablero[p][z] == L && tablero[p][z+1] == P && tablero[p][z+2] == P && tablero[p][z+3] == L){
        
        if(item == 'item'+b+''){
                chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = 'Fabrica';
document.getElementById('item'+b+'').style.background = '#751C0E'; tablero[p-1][z] = 'Fabrica'; // M primera
        
        
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';        
document.getElementById('item'+a+'').style.background = '#F0E68C'; tablero[p][z+3] = '0'; // L ultima       
        
        
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';     document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';
tablero[p][z+2] = '0'; // P segunda        
        
        
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';     document.getElementById('item'+(a-2)+'').style.background = '#F0E68C';   
tablero[p][z+1] = '0'; // P primera        
        
        
document.getElementById('item'+(a-3)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-3)+'').innerHTML = '';     document.getElementById('item'+(a-3)+'').style.background = '#F0E68C';   
tablero[p][z] = '0';  // L primera      
        
    }else if(item == 'item'+a+''){
        chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';
document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[p-1][z] = '0'; // M primera        

document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = 'Fabrica';        
document.getElementById('item'+a+'').style.background = '#751C0E'; tablero[p][z+3] = 'Fabrica'; // L ultima        
        
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';     document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';
tablero[p][z+2] = '0'; // P segunda 

document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';     document.getElementById('item'+(a-2)+'').style.background = '#F0E68C';   
tablero[p][z+1] = '0'; // P primera 
        
document.getElementById('item'+(a-3)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-3)+'').innerHTML = '';     document.getElementById('item'+(a-3)+'').style.background = '#F0E68C';   
tablero[p][z] = '0';  // L primera         
    
    }else if(item == 'item'+(a-1)+''){
             chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';
document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[p-1][z] = '0'; // M primera        

document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';        
document.getElementById('item'+a+'').style.background = '#F0E68C'; tablero[p][z+3] = '0'; // L ultima        
        
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = 'Fabrica';     document.getElementById('item'+(a-1)+'').style.background = '#751C0E';
tablero[p][z+2] = 'Fabrica'; // P segunda
        
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';     document.getElementById('item'+(a-2)+'').style.background = '#F0E68C';   
tablero[p][z+1] = '0'; // P primera  
        
document.getElementById('item'+(a-3)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-3)+'').innerHTML = '';     document.getElementById('item'+(a-3)+'').style.background = '#F0E68C';   
tablero[p][z] = '0';  // L primera        
        
    }else if(item == 'item'+(a-2)+''){
             chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';
document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[p-1][z] = '0'; // M primera             

document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';        
document.getElementById('item'+a+'').style.background = '#F0E68C'; tablero[p][z+3] = '0'; // L ultima  
        
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';     document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';
tablero[p][z+2] = '0'; // P segunda
        
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = 'Fabrica';     document.getElementById('item'+(a-2)+'').style.background = '#751C0E';   
tablero[p][z+1] = 'Fabrica'; // P primera
        
document.getElementById('item'+(a-3)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-3)+'').innerHTML = '';     document.getElementById('item'+(a-3)+'').style.background = '#F0E68C';   
tablero[p][z] = '0';  // L primera        
        
    }else if(item == 'item'+(a-3)+''){
            chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';
document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[p-1][z] = '0'; // M primera        
             
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';        
document.getElementById('item'+a+'').style.background = '#F0E68C'; tablero[p][z+3] = '0'; // L ultima 
        
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';     document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';
tablero[p][z+2] = '0'; // P segunda
        
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';     document.getElementById('item'+(a-2)+'').style.background = '#F0E68C';   
tablero[p][z+1] = '0'; // P primera
        
document.getElementById('item'+(a-3)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-3)+'').innerHTML = 'Fabrica';     document.getElementById('item'+(a-3)+'').style.background = '#751C0E';   
tablero[p][z] = 'Fabrica';  // L primera        
    }else{}    
        
    }else{}
        a+=4;
        b+=4;
    }
    
    
    //=====================PATRON 2
    a = 8, b = 4;
    
    if(chec == 0){
       
    for(var p=1; p<=3; p++){
    var z = 0;
        
    if(tablero[p-1][z+3] == M && tablero[p][z] == L && tablero[p][z+1] == P && tablero[p][z+2] == P && tablero[p][z+3] == L){    
    if(item == 'item'+b+''){
       chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = 'Fabrica';        
document.getElementById('item'+b+'').style.background = '#751C0E'; tablero[p-1][z+3] = 'Fabrica'; // M primera      
        
        
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';        
document.getElementById('item'+a+'').style.background = '#F0E68C'; 
tablero[p][z+3] = '0'; 
        
        
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';     document.getElementById('item'+(a-1)+'').style.background = '#F0E68C'; tablero[p][z+2] = '0';       
        
        
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';     document.getElementById('item'+(a-2)+'').style.background = '#F0E68C'; tablero[p][z+1] = '0';  
        
        
document.getElementById('item'+(a-3)+'').style.border = '2px solid blue';        
document.getElementById('item'+(a-3)+'').innerHTML = '';     document.getElementById('item'+(a-3)+'').style.background = '#F0E68C'; tablero[p][z] = '0';        
       
    }else if(item == 'item'+a+''){
             chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';        
document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[p-1][z+3] = '0'; // M primera   
        
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = 'Fabrica';        
document.getElementById('item'+a+'').style.background = '#751C0E'; 
tablero[p][z+3] = 'Fabrica';        
        
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';     document.getElementById('item'+(a-1)+'').style.background = '#F0E68C'; tablero[p][z+2] = '0';

document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';     document.getElementById('item'+(a-2)+'').style.background = '#F0E68C'; tablero[p][z+1] = '0'; 
        
document.getElementById('item'+(a-3)+'').style.border = '';        
document.getElementById('item'+(a-3)+'').innerHTML = '';     document.getElementById('item'+(a-3)+'').style.background = '#F0E68C'; tablero[p][z] = '0';
        
    }else if(item == 'item'+(a-1)+''){
             chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';        
document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[p-1][z+3] = '0'; // M primera   
        
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';        
document.getElementById('item'+a+'').style.background = '#F0E68C'; 
tablero[p][z+3] = '0';        
        
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = 'Fabrica';     document.getElementById('item'+(a-1)+'').style.background = '#751C0E'; tablero[p][z+2] = 'Fabrica';

document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';     document.getElementById('item'+(a-2)+'').style.background = '#F0E68C'; tablero[p][z+1] = '0'; 
        
document.getElementById('item'+(a-3)+'').style.border = '';        
document.getElementById('item'+(a-3)+'').innerHTML = '';     document.getElementById('item'+(a-3)+'').style.background = '#F0E68C'; tablero[p][z] = '0';             
             
             
    }else if(item == 'item'+(a-2)+''){
             chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';        
document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[p-1][z+3] = '0'; // M primera   
        
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';        
document.getElementById('item'+a+'').style.background = '#F0E68C'; 
tablero[p][z+3] = '0';        
        
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';     document.getElementById('item'+(a-1)+'').style.background = '#F0E68C'; tablero[p][z+2] = '0';

document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = 'Fabrica';     document.getElementById('item'+(a-2)+'').style.background = '#751C0E'; tablero[p][z+1] = 'Fabrica'; 
        
document.getElementById('item'+(a-3)+'').style.border = '';        
document.getElementById('item'+(a-3)+'').innerHTML = '';     document.getElementById('item'+(a-3)+'').style.background = '#F0E68C'; tablero[p][z] = '0';             
             
    }else if(item == 'item'+(a-3)+''){
             chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';        
document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[p-1][z+3] = '0'; // M primera   
        
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';        
document.getElementById('item'+a+'').style.background = '#F0E68C'; 
tablero[p][z+3] = '0';        
        
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';     document.getElementById('item'+(a-1)+'').style.background = '#F0E68C'; tablero[p][z+2] = '0';

document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';     document.getElementById('item'+(a-2)+'').style.background = '#F0E68C'; tablero[p][z+1] = '0'; 
        
document.getElementById('item'+(a-3)+'').style.border = '';        
document.getElementById('item'+(a-3)+'').innerHTML = 'Fabrica';     document.getElementById('item'+(a-3)+'').style.background = '#751C0E'; tablero[p][z] = 'Fabrica';             
             
             
    }else{}    
       
    }else{}
      a+=4; b+=4;
    
    }
    }else{}
    
    //=======================PATRON 3
    a = 4, b = 8;
    
    if(chec == 0){
    for(var p=1; p<=3; p++){    
            var z=0;
    
    if(tablero[p-1][z] == L && tablero[p-1][z+1] == P && tablero[p-1][z+2] == P && tablero[p-1][z+3] == L && tablero[p][z+3] == M){
        
        if(item == 'item'+b+''){
            chec++;
        document.getElementById('item'+b+'').style.border = '2px solid blue';
        document.getElementById('item'+b+'').innerHTML = 'Fabrica'; document.getElementById('item'+b+'').style.background = '#751C0E'; tablero[p][z+3] = 'Fabrica';    
            
        document.getElementById('item'+a+'').style.border = '2px solid blue';
        document.getElementById('item'+a+'').innerHTML = '';
        document.getElementById('item'+a+'').style.background = '#F0E68C';  tablero[p-1][z+3] = '0';    
            
        document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
        document.getElementById('item'+(a-1)+'').innerHTML = '';   document.getElementById('item'+(a-1)+'').style.background = '#F0E68C'; tablero[p-1][z+2] = '0';    
        
        document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
        document.getElementById('item'+(a-2)+'').innerHTML = '';    document.getElementById('item'+(a-2)+'').style.background = '#F0E68C'; tablero[p-1][z+1] = '0';    
        
        document.getElementById('item'+(a-3)+'').style.border = '2px solid blue';
        document.getElementById('item'+(a-3)+'').innerHTML = '';   document.getElementById('item'+(a-3)+'').style.background = '#F0E68C'; tablero[p-1][z] = '0';    
            
        }else if(item == 'item'+a+''){
        chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = ''; document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[p][z+3] = '0';            


document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = 'Fabrica';
document.getElementById('item'+a+'').style.background = '#751C0E';  tablero[p-1][z+3] = 'Fabrica';            
            

document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';   document.getElementById('item'+(a-1)+'').style.background = '#F0E68C'; tablero[p-1][z+2] = '0'; 
            
            
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';   document.getElementById('item'+(a-2)+'').style.background = '#F0E68C'; tablero[p-1][z+1] = '0';
            

document.getElementById('item'+(a-3)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-3)+'').innerHTML = '';   document.getElementById('item'+(a-3)+'').style.background = '#F0E68C'; tablero[p-1][z] = '0';            
                 
        }else if(item == 'item'+(a-1)+''){
        chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = ''; document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[p][z+3] = '0';                 
        
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';
document.getElementById('item'+a+'').style.background = '#F0E68C';  tablero[p-1][z+3] = '0'; 
            
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = 'Fabrica';   document.getElementById('item'+(a-1)+'').style.background = '#751C0E'; tablero[p-1][z+2] = 'Fabrica';
            
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';   document.getElementById('item'+(a-2)+'').style.background = '#F0E68C'; tablero[p-1][z+1] = '0';
            
document.getElementById('item'+(a-3)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-3)+'').innerHTML = '';   document.getElementById('item'+(a-3)+'').style.background = '#F0E68C'; tablero[p-1][z] = '0';                        
                 
        }else if(item == 'item'+(a-2)+''){
        chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = ''; document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[p][z+3] = '0';                 
        
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';
document.getElementById('item'+a+'').style.background = '#F0E68C';  tablero[p-1][z+3] = '0'; 
            
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';   document.getElementById('item'+(a-1)+'').style.background = '#F0E68C'; tablero[p-1][z+2] = '0';
            
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = 'Fabrica';   document.getElementById('item'+(a-2)+'').style.background = '#751C0E'; tablero[p-1][z+1] = 'Fabrica';
            
document.getElementById('item'+(a-3)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-3)+'').innerHTML = '';   document.getElementById('item'+(a-3)+'').style.background = '#F0E68C'; tablero[p-1][z] = '0';                 
                 
                 
        }else if(item == 'item'+(a-3)+''){
        chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = ''; document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[p][z+3] = '0';                 
        
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';
document.getElementById('item'+a+'').style.background = '#F0E68C';  tablero[p-1][z+3] = '0'; 
            
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';   document.getElementById('item'+(a-1)+'').style.background = '#F0E68C'; tablero[p-1][z+2] = '0';
            
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';   document.getElementById('item'+(a-2)+'').style.background = '#F0E68C'; tablero[p-1][z+1] = '0';
            
document.getElementById('item'+(a-3)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-3)+'').innerHTML = 'Fabrica';   document.getElementById('item'+(a-3)+'').style.background = '#751C0E'; tablero[p-1][z] = 'Fabrica';          
                 
                 
        }else{}
        
    }else{}
    
        a+=4; b+=4;
        
    }
    }else{} 
    
    //=========================PATRON 4
    a = 4, b = 5;
    
    if(chec == 0){
       
    for(var p=1; p<=3; p++){
     var z = 0;
    if(tablero[p-1][z] == L && tablero[p-1][z+1] == P && tablero[p-1][z+2] == P && tablero[p-1][z+3] == L && tablero[p][z] == M){    
    if(item == 'item'+b+''){
       chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = 'Fabrica';        
document.getElementById('item'+b+'').style.background = '#751C0E'; tablero[p][z] = 'Fabrica';
        
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';        
document.getElementById('item'+a+'').style.background = '#F0E68C'; 
tablero[p-1][z+3] = '0';        
       
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';     document.getElementById('item'+(a-1)+'').style.background = '#F0E68C'; tablero[p-1][z+2] = '0';

document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';     document.getElementById('item'+(a-2)+'').style.background = '#F0E68C'; tablero[p-1][z+1] = '0';  
        
document.getElementById('item'+(a-3)+'').style.border = '2px solid blue';        
document.getElementById('item'+(a-3)+'').innerHTML = '';     document.getElementById('item'+(a-3)+'').style.background = '#F0E68C'; tablero[p-1][z] = '0';        
        
       }else if(item == 'item'+a+''){
       chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';        
document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[p][z] = '0';        
           
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = 'Fabrica';        
document.getElementById('item'+a+'').style.background = '#751C0E'; 
tablero[p-1][z+3] = 'Fabrica';          

document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';     document.getElementById('item'+(a-1)+'').style.background = '#F0E68C'; tablero[p-1][z+2] = '0';           
       
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';     document.getElementById('item'+(a-2)+'').style.background = '#F0E68C'; tablero[p-1][z+1] = '0'; 
           
document.getElementById('item'+(a-3)+'').style.border = '2px solid blue';        
document.getElementById('item'+(a-3)+'').innerHTML = '';     document.getElementById('item'+(a-3)+'').style.background = '#F0E68C'; tablero[p-1][z] = '0';
           
        }else if(item == 'item'+(a-1)+''){
       chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';        
document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[p][z] = '0';
            
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';        
document.getElementById('item'+a+'').style.background = '#F0E68C'; 
tablero[p-1][z+3] = '0';            
            
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = 'Fabrica';     document.getElementById('item'+(a-1)+'').style.background = '#751C0E'; tablero[p-1][z+2] = 'Fabrica'; 
            
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';     document.getElementById('item'+(a-2)+'').style.background = '#F0E68C'; tablero[p-1][z+1] = '0';
            
document.getElementById('item'+(a-3)+'').style.border = '2px solid blue';        
document.getElementById('item'+(a-3)+'').innerHTML = '';     document.getElementById('item'+(a-3)+'').style.background = '#F0E68C'; tablero[p-1][z] = '0';
            
        }else if(item == 'item'+(a-2)+''){
       chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';        
document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[p][z] = '0';
            
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';        
document.getElementById('item'+a+'').style.background = '#F0E68C'; 
tablero[p-1][z+3] = '0';
            
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';     document.getElementById('item'+(a-1)+'').style.background = '#F0E68C'; tablero[p-1][z+2] = '0'; 
            
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = 'Fabrica';     document.getElementById('item'+(a-2)+'').style.background = '#751C0E'; tablero[p-1][z+1] = 'Fabrica'; 
            
document.getElementById('item'+(a-3)+'').style.border = '2px solid blue';        
document.getElementById('item'+(a-3)+'').innerHTML = '';     document.getElementById('item'+(a-3)+'').style.background = '#F0E68C'; tablero[p-1][z] = '0';            
                 
        }else if(item == 'item'+(a-3)+''){
       chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';        
document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[p][z] = '0';
            
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';        
document.getElementById('item'+a+'').style.background = '#F0E68C'; 
tablero[p-1][z+3] = '0';
            
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';     document.getElementById('item'+(a-1)+'').style.background = '#F0E68C'; tablero[p-1][z+2] = '0'; 
            
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';     document.getElementById('item'+(a-2)+'').style.background = '#F0E68C'; tablero[p-1][z+1] = '0'; 
            
document.getElementById('item'+(a-3)+'').style.border = '2px solid blue';        
document.getElementById('item'+(a-3)+'').innerHTML = 'Fabrica';     document.getElementById('item'+(a-3)+'').style.background = '#751C0E'; tablero[p-1][z] = 'Fabrica';                 
                 
        }else{}    
        
    }else{}
        a+=4; b+=4;
        
    }
    }else{}
    
    //====================PATRON 5
    a = 2, b = 13;
    if(chec == 0){
       
    for(var l=1; l<=3; l++){ 
       var i=0;
    if(tablero[i][l] == L && tablero[i+1][l] == P && tablero[i+2][l] == P && tablero[i+3][l] == L && tablero[i+3][l-1] == M){
        
        if(item == 'item'+b+''){
          chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = 'Fabrica';        
document.getElementById('item'+b+'').style.background = '#751C0E'; tablero[i+3][l-1] = 'Fabrica'; 
            
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';        
document.getElementById('item'+a+'').style.background = '#F0E68C'; 
tablero[i][l] = '0'; 
            
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';     document.getElementById('item'+(a+4)+'').style.background = '#F0E68C'; tablero[i+1][l] = '0'; 
            
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';     document.getElementById('item'+(a+8)+'').style.background = '#F0E68C'; tablero[i+2][l] = '0'; 
            
document.getElementById('item'+(a+12)+'').style.border = '2px solid blue';        
document.getElementById('item'+(a+12)+'').innerHTML = '';     document.getElementById('item'+(a+12)+'').style.background = '#F0E68C'; tablero[i+3][l] = '0';            
           
        }else if(item == 'item'+a+''){
          chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';        
document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[i+3][l-1] = '0';
            
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = 'Fabrica';        
document.getElementById('item'+a+'').style.background = '#751C0E'; 
tablero[i][l] = 'Fabrica'; 
            
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';     document.getElementById('item'+(a+4)+'').style.background = '#F0E68C'; tablero[i+1][l] = '0';
            
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';     document.getElementById('item'+(a+8)+'').style.background = '#F0E68C'; tablero[i+2][l] = '0';
            
document.getElementById('item'+(a+12)+'').style.border = '2px solid blue';        
document.getElementById('item'+(a+12)+'').innerHTML = '';     document.getElementById('item'+(a+12)+'').style.background = '#F0E68C'; tablero[i+3][l] = '0';            
            
                 
        }else if(item == 'item'+(a+4)+''){
          chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';        
document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[i+3][l-1] = '0'; 
            
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';        
document.getElementById('item'+a+'').style.background = '#F0E68C'; 
tablero[i][l] = '0'; 
            
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = 'Fabrica';     document.getElementById('item'+(a+4)+'').style.background = '#751C0E'; tablero[i+1][l] = 'Fabrica'; 
            
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';     document.getElementById('item'+(a+8)+'').style.background = '#F0E68C'; tablero[i+2][l] = '0'; 
            
document.getElementById('item'+(a+12)+'').style.border = '2px solid blue';        
document.getElementById('item'+(a+12)+'').innerHTML = '';     document.getElementById('item'+(a+12)+'').style.background = '#F0E68C'; tablero[i+3][l] = '0';            
             
            
                 
        }else if(item == 'item'+(a+8)+''){
          chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';        
document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[i+3][l-1] = '0'; 
            
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';        
document.getElementById('item'+a+'').style.background = '#F0E68C'; 
tablero[i][l] = '0'; 
            
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';     document.getElementById('item'+(a+4)+'').style.background = '#F0E68C'; tablero[i+1][l] = '0'; 
            
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = 'Fabrica';     document.getElementById('item'+(a+8)+'').style.background = '#751C0E'; tablero[i+2][l] = 'Fabrica'; 
            
document.getElementById('item'+(a+12)+'').style.border = '2px solid blue';        
document.getElementById('item'+(a+12)+'').innerHTML = '';     document.getElementById('item'+(a+12)+'').style.background = '#F0E68C'; tablero[i+3][l] = '0';                 
                 
                 
        }else if(item == 'item'+(a+12)+''){
          chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';        
document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[i+3][l-1] = '0'; 
            
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';        
document.getElementById('item'+a+'').style.background = '#F0E68C'; 
tablero[i][l] = '0'; 
            
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';     document.getElementById('item'+(a+4)+'').style.background = '#F0E68C'; tablero[i+1][l] = '0'; 
            
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';     document.getElementById('item'+(a+8)+'').style.background = '#F0E68C'; tablero[i+2][l] = '0'; 
            
document.getElementById('item'+(a+12)+'').style.border = '2px solid blue';        
document.getElementById('item'+(a+12)+'').innerHTML = 'Fabrica';     document.getElementById('item'+(a+12)+'').style.background = '#751C0E'; tablero[i+3][l] = 'Fabrica';                 
                 
        }else{}
        
    }else{}
        a++; b++;
    }
    }else{}
    
    //====================PATRON 6
    
    a = 2, b = 1;
    if(chec == 0){
    for(var l=1; l<=3; l++){
           var i=0;
    
    if(tablero[i][l] == L && tablero[i+1][l] == P && tablero[i+2][l] == P && tablero[i+3][l] == L && tablero[i][l-1] == M){    
        
        if(item == 'item'+b+''){
        chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = 'Fabrica';        
document.getElementById('item'+b+'').style.background = '#751C0E'; tablero[i][l-1] = 'Fabrica';
            
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';        
document.getElementById('item'+a+'').style.background = '#F0E68C'; 
tablero[i][l] = '0';
            
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';     document.getElementById('item'+(a+4)+'').style.background = '#F0E68C'; tablero[i+1][l] = '0';            
           
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';     document.getElementById('item'+(a+8)+'').style.background = '#F0E68C'; tablero[i+2][l] = '0'; 
            
document.getElementById('item'+(a+12)+'').style.border = '2px solid blue';        
document.getElementById('item'+(a+12)+'').innerHTML = '';     document.getElementById('item'+(a+12)+'').style.background = '#F0E68C'; tablero[i+3][l] = '0';
            
        }else if(item == 'item'+a+''){
        chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';        
document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[i][l-1] = '0';
            
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = 'Fabrica';        
document.getElementById('item'+a+'').style.background = '#751C0E'; 
tablero[i][l] = 'Fabrica';
            
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';     document.getElementById('item'+(a+4)+'').style.background = '#F0E68C'; tablero[i+1][l] = '0';            
           
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';     document.getElementById('item'+(a+8)+'').style.background = '#F0E68C'; tablero[i+2][l] = '0'; 
            
document.getElementById('item'+(a+12)+'').style.border = '2px solid blue';        
document.getElementById('item'+(a+12)+'').innerHTML = '';     document.getElementById('item'+(a+12)+'').style.background = '#F0E68C'; tablero[i+3][l] = '0';                 
                 
                 
        }else if(item == 'item'+(a+4)+''){
        chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';        
document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[i][l-1] = '0';
            
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';        
document.getElementById('item'+a+'').style.background = '#F0E68C'; 
tablero[i][l] = '0';
            
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = 'Fabrica';     document.getElementById('item'+(a+4)+'').style.background = '#751C0E'; tablero[i+1][l] = 'Fabrica';            
           
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';     document.getElementById('item'+(a+8)+'').style.background = '#F0E68C'; tablero[i+2][l] = '0'; 
            
document.getElementById('item'+(a+12)+'').style.border = '2px solid blue';        
document.getElementById('item'+(a+12)+'').innerHTML = '';     document.getElementById('item'+(a+12)+'').style.background = '#F0E68C'; tablero[i+3][l] = '0';                 
                 
        }else if(item == 'item'+(a+8)+''){
        chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';        
document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[i][l-1] = '0';
            
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';        
document.getElementById('item'+a+'').style.background = '#F0E68C'; 
tablero[i][l] = '0';
            
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';     document.getElementById('item'+(a+4)+'').style.background = '#F0E68C'; tablero[i+1][l] = '0';            
           
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = 'Fabrica';     document.getElementById('item'+(a+8)+'').style.background = '#751C0E'; tablero[i+2][l] = 'Fabrica'; 
            
document.getElementById('item'+(a+12)+'').style.border = '2px solid blue';        
document.getElementById('item'+(a+12)+'').innerHTML = '';     document.getElementById('item'+(a+12)+'').style.background = '#F0E68C'; tablero[i+3][l] = '0';                 
                 
        }else if(item == 'item'+(a+12)+''){
        chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';        
document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[i][l-1] = '0';
            
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';        
document.getElementById('item'+a+'').style.background = '#F0E68C'; 
tablero[i][l] = '0';
            
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';     document.getElementById('item'+(a+4)+'').style.background = '#F0E68C'; tablero[i+1][l] = '0';            
           
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';     document.getElementById('item'+(a+8)+'').style.background = '#F0E68C'; tablero[i+2][l] = '0'; 
            
document.getElementById('item'+(a+12)+'').style.border = '2px solid blue';        
document.getElementById('item'+(a+12)+'').innerHTML = 'Fabrica';     document.getElementById('item'+(a+12)+'').style.background = '#751C0E'; tablero[i+3][l] = 'Fabrica';         
            
                 
        }else{}
       
    }else{}
      
        a++; b++;
    }
    }else{}
    
    //======================PATRON 7
    a =1, b = 2;
    if(chec == 0){
    for(var l=1; l<=3; l++){
           var i=0;
    
    if(tablero[i][l-1] == L && tablero[i+1][l-1] == P && tablero[i+2][l-1] == P && tablero[i+3][l-1] == L && tablero[i][l] == M){
        
       if(item == 'item'+b+''){
          chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = 'Fabrica';        
document.getElementById('item'+b+'').style.background = '#751C0E'; tablero[i][l] = 'Fabrica';
           
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';        
document.getElementById('item'+a+'').style.background = '#F0E68C'; 
tablero[i][l-1] = '0';           

document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';     document.getElementById('item'+(a+4)+'').style.background = '#F0E68C'; tablero[i+1][l-1] = '0';
           
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';     document.getElementById('item'+(a+8)+'').style.background = '#F0E68C'; tablero[i+2][l-1] = '0';
           
document.getElementById('item'+(a+12)+'').style.border = '2px solid blue';        
document.getElementById('item'+(a+12)+'').innerHTML = '';     document.getElementById('item'+(a+12)+'').style.background = '#F0E68C'; tablero[i+3][l-1] = '0';           
       
       }else if(item == 'item'+a+''){
          chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';        
document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[i][l] = '0';
           
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = 'Fabrica';        
document.getElementById('item'+a+'').style.background = '#751C0E'; 
tablero[i][l-1] = 'Fabrica';           

document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';     document.getElementById('item'+(a+4)+'').style.background = '#F0E68C'; tablero[i+1][l-1] = '0';
           
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';     document.getElementById('item'+(a+8)+'').style.background = '#F0E68C'; tablero[i+2][l-1] = '0';
           
document.getElementById('item'+(a+12)+'').style.border = '2px solid blue';        
document.getElementById('item'+(a+12)+'').innerHTML = '';     document.getElementById('item'+(a+12)+'').style.background = '#F0E68C'; tablero[i+3][l-1] = '0';                
                
        }else if(item == 'item'+(a+4)+''){
          chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';        
document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[i][l] = '0';
           
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';        
document.getElementById('item'+a+'').style.background = '#F0E68C'; 
tablero[i][l-1] = '0';           

document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = 'Fabrica';     document.getElementById('item'+(a+4)+'').style.background = '#751C0E'; tablero[i+1][l-1] = 'Fabrica';
           
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';     document.getElementById('item'+(a+8)+'').style.background = '#F0E68C'; tablero[i+2][l-1] = '0';
           
document.getElementById('item'+(a+12)+'').style.border = '2px solid blue';        
document.getElementById('item'+(a+12)+'').innerHTML = '';     document.getElementById('item'+(a+12)+'').style.background = '#F0E68C'; tablero[i+3][l-1] = '0';                 
                 
        }else if(item == 'item'+(a+8)+''){
          chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';        
document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[i][l] = '0';
           
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';        
document.getElementById('item'+a+'').style.background = '#F0E68C'; 
tablero[i][l-1] = '0';           

document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';     document.getElementById('item'+(a+4)+'').style.background = '#F0E68C'; tablero[i+1][l-1] = '0';
           
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = 'Fabrica';     document.getElementById('item'+(a+8)+'').style.background = '#751C0E'; tablero[i+2][l-1] = 'Fabrica';
           
document.getElementById('item'+(a+12)+'').style.border = '2px solid blue';        
document.getElementById('item'+(a+12)+'').innerHTML = '';     document.getElementById('item'+(a+12)+'').style.background = '#F0E68C'; tablero[i+3][l-1] = '0';                 
                 
        }else if(item == 'item'+(a+12)+''){
          chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';        
document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[i][l] = '0';
           
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';        
document.getElementById('item'+a+'').style.background = '#F0E68C'; 
tablero[i][l-1] = '0';           

document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';     document.getElementById('item'+(a+4)+'').style.background = '#F0E68C'; tablero[i+1][l-1] = '0';
           
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';     document.getElementById('item'+(a+8)+'').style.background = '#F0E68C'; tablero[i+2][l-1] = '0';
           
document.getElementById('item'+(a+12)+'').style.border = '2px solid blue';        
document.getElementById('item'+(a+12)+'').innerHTML = 'Fabrica';     document.getElementById('item'+(a+12)+'').style.background = '#751C0E'; tablero[i+3][l-1] = 'Fabrica';            
            
                 
        }else{} 
        
    
    }else{}
            a++; b++;
        
    }
    }else{}
    
    //=======================PATRON 8
    a = 1, b = 14;
    
    if(chec == 0){
    for(var l=1; l<=3; l++){
           var i=0;
    
    if(tablero[i][l-1] == L && tablero[i+1][l-1] == P && tablero[i+2][l-1] == P && tablero[i+3][l-1] == L && tablero[i+3][l] == M){    
        
        if(item == 'item'+b+''){
          chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = 'Fabrica';        
document.getElementById('item'+b+'').style.background = '#751C0E'; tablero[i+3][l] = 'Fabrica';
           
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';        
document.getElementById('item'+a+'').style.background = '#F0E68C'; 
tablero[i][l-1] = '0';           

document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';     document.getElementById('item'+(a+4)+'').style.background = '#F0E68C'; tablero[i+1][l-1] = '0';
           
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';     document.getElementById('item'+(a+8)+'').style.background = '#F0E68C'; tablero[i+2][l-1] = '0';
           
document.getElementById('item'+(a+12)+'').style.border = '2px solid blue';        
document.getElementById('item'+(a+12)+'').innerHTML = '';     document.getElementById('item'+(a+12)+'').style.background = '#F0E68C'; tablero[i+3][l-1] = '0';           
           
         }else if(item == 'item'+a+''){
          chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';        
document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[i+3][l] = '0';
           
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = 'Fabrica';        
document.getElementById('item'+a+'').style.background = '#751C0E'; 
tablero[i][l-1] = 'Fabrica';           

document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';     document.getElementById('item'+(a+4)+'').style.background = '#F0E68C'; tablero[i+1][l-1] = '0';
           
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';     document.getElementById('item'+(a+8)+'').style.background = '#F0E68C'; tablero[i+2][l-1] = '0';
           
document.getElementById('item'+(a+12)+'').style.border = '2px solid blue';        
document.getElementById('item'+(a+12)+'').innerHTML = '';     document.getElementById('item'+(a+12)+'').style.background = '#F0E68C'; tablero[i+3][l-1] = '0';                  
                  
        }else if(item == 'item'+(a+4)+''){
          chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';        
document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[i+3][l] = '0';
           
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';        
document.getElementById('item'+a+'').style.background = '#F0E68C'; 
tablero[i][l-1] = '0';           

document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = 'Fabrica';     document.getElementById('item'+(a+4)+'').style.background = '#751C0E'; tablero[i+1][l-1] = 'Fabrica';
           
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';     document.getElementById('item'+(a+8)+'').style.background = '#F0E68C'; tablero[i+2][l-1] = '0';
           
document.getElementById('item'+(a+12)+'').style.border = '2px solid blue';        
document.getElementById('item'+(a+12)+'').innerHTML = '';     document.getElementById('item'+(a+12)+'').style.background = '#F0E68C'; tablero[i+3][l-1] = '0';                 
                 
        }else if(item == 'item'+(a+8)+''){
          chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';        
document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[i+3][l] = '0';
           
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';        
document.getElementById('item'+a+'').style.background = '#F0E68C'; 
tablero[i][l-1] = '0';           

document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';     document.getElementById('item'+(a+4)+'').style.background = '#F0E68C'; tablero[i+1][l-1] = '0';
           
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = 'Fabrica';     document.getElementById('item'+(a+8)+'').style.background = '#751C0E'; tablero[i+2][l-1] = 'Fabrica';
           
document.getElementById('item'+(a+12)+'').style.border = '2px solid blue';        
document.getElementById('item'+(a+12)+'').innerHTML = '';     document.getElementById('item'+(a+12)+'').style.background = '#F0E68C'; tablero[i+3][l-1] = '0';                 
                 
        }else if(item == 'item'+(a+12)+''){
          chec++;
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';        
document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[i+3][l] = '0';
           
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';        
document.getElementById('item'+a+'').style.background = '#F0E68C'; 
tablero[i][l-1] = '0';           

document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';     document.getElementById('item'+(a+4)+'').style.background = '#F0E68C'; tablero[i+1][l-1] = '0';
           
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';     document.getElementById('item'+(a+8)+'').style.background = '#F0E68C'; tablero[i+2][l-1] = '0';
           
document.getElementById('item'+(a+12)+'').style.border = '2px solid blue';        
document.getElementById('item'+(a+12)+'').innerHTML = 'Fabrica';     document.getElementById('item'+(a+12)+'').style.background = '#751C0E'; tablero[i+3][l-1] = 'Fabrica';                 
                 
        }else{}
        
    }else{}
        a++; b++;
    }
    }else{}
    
        for(var i=1; i<=16; i++){
        document.getElementById('item'+i+'').style.border = '2px solid blue';
    }
}

function ConstruirPasteleria(item){
  var T = 'Trigo', L = 'Ladrillo', V = 'Vidrio';
    var chec = 0;
    var a = 7, b = 2;
    
    //==============PATRON 1
    for(var p=1; p<=3; p++){
        
        for(var k=0; k<=1; k++){
            
        if(tablero[p-1][k+1] == T && tablero[p][k] == L && tablero[p][k+1] == V && tablero[p][k+2] == L){ 
            
        if(item == 'item'+a+''){
           chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = 'Pasteleria';      document.getElementById('item'+a+'').style.background = '#751C0E'; tablero[p-1][k+2] = 'Pasteleria';            

document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';     document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0';            

document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';     document.getElementById('item'+(a-2)+'').style.background = '#F0E68C';   tablero[p][k] = '0';            

document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';         document.getElementById('item'+b+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0';            
          
           }else if(item == 'item'+(a-1)+''){
           chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';         document.getElementById('item'+a+'').style.background = '#F0E68C'; tablero[p-1][k+2] = '0';            

document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = 'Pasteleria';     document.getElementById('item'+(a-1)+'').style.background = '#751C0E';   tablero[p-1][k+1] = 'Pasteleria';            

document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';     document.getElementById('item'+(a-2)+'').style.background = '#F0E68C';   tablero[p][k] = '0';            

document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';         document.getElementById('item'+b+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0';             
               
                    
           }else if(item == 'item'+(a-2)+''){
           chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';         document.getElementById('item'+a+'').style.background = '#F0E68C'; tablero[p-1][k+2] = '0';            

document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';     document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0';            

document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = 'Pasteleria';     document.getElementById('item'+(a-2)+'').style.background = '#751C0E';   tablero[p][k] = 'Pasteleria';            

document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';         document.getElementById('item'+b+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0';                    
                    
                    
           }else if(item == 'item'+b+''){
           chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';         document.getElementById('item'+a+'').style.background = '#F0E68C'; tablero[p-1][k+2] = '0';            

document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';     document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0';            

document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';     document.getElementById('item'+(a-2)+'').style.background = '#F0E68C';   tablero[p][k] = '0';            

document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = 'Pasteleria';         document.getElementById('item'+b+'').style.background = '#751C0E';   tablero[p-1][k+1] = 'Pasteleria';                    
               
                    
            }else{}
        
        }else{}
            
           a++; b++; 
        }
        a+=2; b+=2;
    }
    
    //====================PATRON 2
    a = 3, b = 6;
    if(chec == 0){
    for(var p=1; p<=3; p++){
        
        for(var k=0; k<=1; k++){   
       
        if(tablero[p-1][k] == L && tablero[p-1][k+1] == V && tablero[p-1][k+2] == L && tablero[p][k+1] == T){    
        
        if(item == 'item'+a+''){
            chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = 'Pasteleria';     document.getElementById('item'+a+'').style.background = '#751C0E'; tablero[p-1][k+2] = 'Pasteleria';           
            
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';     document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0';
            
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';     document.getElementById('item'+(a-2)+'').style.background = '#F0E68C';   tablero[p-1][k] = '0';   
 
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';         document.getElementById('item'+b+'').style.background = '#F0E68C';   tablero[p][k+1] = '0';            
           
        }else if(item == 'item'+(a-1)+''){
            chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';     document.getElementById('item'+a+'').style.background = '#F0E68C'; tablero[p-1][k+2] = '0';           
            
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = 'Pasteleria';     document.getElementById('item'+(a-1)+'').style.background = '#751C0E';   tablero[p-1][k+1] = 'Pasteleria';
            
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';     document.getElementById('item'+(a-2)+'').style.background = '#F0E68C';   tablero[p-1][k] = '0';   
 
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';         document.getElementById('item'+b+'').style.background = '#F0E68C';   tablero[p][k+1] = '0';                 
                 
                 
        }else if(item == 'item'+(a-2)+''){
            chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';     document.getElementById('item'+a+'').style.background = '#F0E68C'; tablero[p-1][k+2] = '0';           
            
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';     document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0';
            
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = 'Pasteleria';     document.getElementById('item'+(a-2)+'').style.background = '#751C0E';   tablero[p-1][k] = 'Pasteleria';   
 
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';         document.getElementById('item'+b+'').style.background = '#F0E68C';   tablero[p][k+1] = '0';                 
                 
                 
        }else if(item == 'item'+b+''){
            chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';     document.getElementById('item'+a+'').style.background = '#F0E68C'; tablero[p-1][k+2] = '0';           
            
document.getElementById('item'+(a-1)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-1)+'').innerHTML = '';     document.getElementById('item'+(a-1)+'').style.background = '#F0E68C';   tablero[p-1][k+1] = '0';
            
document.getElementById('item'+(a-2)+'').style.border = '2px solid blue';
document.getElementById('item'+(a-2)+'').innerHTML = '';     document.getElementById('item'+(a-2)+'').style.background = '#F0E68C';   tablero[p-1][k] = '0';   
 
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = 'Pasteleria';         document.getElementById('item'+b+'').style.background = '#751C0E';   tablero[p][k+1] = 'Pasteleria';                 
         
                 
        }else{}    
            
       
        }else{}
           a++; b++;   
        }
           a+=2; b+=2;
    }
            
    }else{}
    
    //====================PATRON 3
    a = 2, b = 5;
    if(chec == 0){
       
     for(var i=0; i<=1; i++){
        
        for(var l=1; l<=3; l++){
            
        if(tablero[i][l] == L && tablero[i+1][l] == V && tablero[i+2][l] == L && tablero[i+1][l-1] == T){
            
        if(item == 'item'+a+''){
                chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = 'Pasteleria';     document.getElementById('item'+a+'').style.background = '#751C0E'; tablero[i][l] = 'Pasteleria';
            
            
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';     document.getElementById('item'+(a+4)+'').style.background = '#F0E68C'; tablero[i+1][l] = '0';
            

document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';     document.getElementById('item'+(a+8)+'').style.background = '#F0E68C'; tablero[i+2][l] = '0';
            

document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';     document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[i+1][l-1] = '0';            
           
        }else if(item == 'item'+(a+4)+''){
                chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';     document.getElementById('item'+a+'').style.background = '#F0E68C'; tablero[i][l] = '0';
            
            
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = 'Pasteleria';     document.getElementById('item'+(a+4)+'').style.background = '#751C0E'; tablero[i+1][l] = 'Pasteleria';
            

document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';     document.getElementById('item'+(a+8)+'').style.background = '#F0E68C'; tablero[i+2][l] = '0';
            

document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';     document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[i+1][l-1] = '0';                 
                 
        }else if(item == 'item'+(a+8)+''){
                chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';     document.getElementById('item'+a+'').style.background = '#F0E68C'; tablero[i][l] = '0';
            
            
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';     document.getElementById('item'+(a+4)+'').style.background = '#F0E68C'; tablero[i+1][l] = '0';
            

document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = 'Pasteleria';     document.getElementById('item'+(a+8)+'').style.background = '#751C0E'; tablero[i+2][l] = 'Pasteleria';
            

document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';     document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[i+1][l-1] = '0';                 
                 
        }else if(item == 'item'+b+''){
                chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';     document.getElementById('item'+a+'').style.background = '#F0E68C'; tablero[i][l] = '0';
            
            
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';     document.getElementById('item'+(a+4)+'').style.background = '#F0E68C'; tablero[i+1][l] = '0';
            

document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';     document.getElementById('item'+(a+8)+'').style.background = '#F0E68C'; tablero[i+2][l] = '0';
            

document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = 'Pasteleria';     document.getElementById('item'+b+'').style.background = '#751C0E'; tablero[i+1][l-1] = 'Pasteleria';                 
                 
                 
        }else{}    
            
            
        }else{}
          a++; b++;  
        }
         a++; b++;
     }
        
    
    }else{}
    
    
    //======================PATRON 4
    a = 1, b = 6;
    if(chec == 0){
        
    for(var i=0; i<=1; i++){
        
        for(var l=1; l<=3; l++){
            
        if(tablero[i][l-1] == L && tablero[i+1][l-1] == V && tablero[i+2][l-1] == L && tablero[i+1][l] == T){ 
            
            if(item == 'item'+a+''){
                chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = 'Pasteleria';     document.getElementById('item'+a+'').style.background = '#751C0E'; tablero[i][l-1] = 'Pasteleria';
                
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';     document.getElementById('item'+(a+4)+'').style.background = '#F0E68C'; tablero[i+1][l-1] = '0';
                
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';     document.getElementById('item'+(a+8)+'').style.background = '#F0E68C'; tablero[i+2][l-1] = '0';                
                
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';     document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[i+1][l] = '0';                
                
            }else if(item == 'item'+(a+4)+''){
                chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';     document.getElementById('item'+a+'').style.background = '#F0E68C'; tablero[i][l-1] = '0';
                
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = 'Pasteleria';     document.getElementById('item'+(a+4)+'').style.background = '#751C0E'; tablero[i+1][l-1] = 'Pasteleria';
                
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';     document.getElementById('item'+(a+8)+'').style.background = '#F0E68C'; tablero[i+2][l-1] = '0';                
                
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';     document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[i+1][l] = '0';                    
            }else if(item == 'item'+(a+8)+''){
                chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';     document.getElementById('item'+a+'').style.background = '#F0E68C'; tablero[i][l-1] = '0';
                
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';     document.getElementById('item'+(a+4)+'').style.background = '#F0E68C'; tablero[i+1][l-1] = '0';
                
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = 'Pasteleria';     document.getElementById('item'+(a+8)+'').style.background = '#751C0E'; tablero[i+2][l-1] = 'Pasteleria';                
                
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = '';     document.getElementById('item'+b+'').style.background = '#F0E68C'; tablero[i+1][l] = '0';                    
                     
                     
            }else if(item == 'item'+b+''){
                chec++;
document.getElementById('item'+a+'').style.border = '2px solid blue';
document.getElementById('item'+a+'').innerHTML = '';     document.getElementById('item'+a+'').style.background = '#F0E68C'; tablero[i][l-1] = '0';
                
document.getElementById('item'+(a+4)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+4)+'').innerHTML = '';     document.getElementById('item'+(a+4)+'').style.background = '#F0E68C'; tablero[i+1][l-1] = '0';
                
document.getElementById('item'+(a+8)+'').style.border = '2px solid blue';
document.getElementById('item'+(a+8)+'').innerHTML = '';     document.getElementById('item'+(a+8)+'').style.background = '#F0E68C'; tablero[i+2][l-1] = '0';                
                
document.getElementById('item'+b+'').style.border = '2px solid blue';
document.getElementById('item'+b+'').innerHTML = 'Pasteleria';     document.getElementById('item'+b+'').style.background = '#751C0E'; tablero[i+1][l] = 'Pasteleria';                     
            
            }else{}
        
        }else{}
          
            a++; b++;
        }
            a++; b++;
    }
        
    }
    
        for(var i=1; i<=16; i++){
        document.getElementById('item'+i+'').style.border = '2px solid blue';
    }
}

function obtenerNombreDelRecurso(){
    if(color == '#9C690A'){
        recursoEnTablero = 'Madera';
    
    }else if(color == '#D3CA0C'){
        recursoEnTablero = 'Trigo';
        
    }else if(color == '#D67412'){
        recursoEnTablero = 'Ladrillo';
        
    }else if(color == '#AADBDF'){
        recursoEnTablero = 'Vidrio';
        
    }else if(color == '#53686A'){
        recursoEnTablero = 'Piedra';
        
    }
    
}

function actualizarMazo(){
    tomasteRecurso = false;
    //Actualizamos fila de mazo
    var recursoUso = recursoUtilizado;
    
    if(recursoUso == 'recurso0'){
        //Elimnamos recurso y lo pasamos al final.
        carta = mazorobo.splice(0,1);
        mazorobo.push(carta);
    }else if(recursoUso == 'recurso1'){
        carta = mazorobo.splice(1,1);
        mazorobo.push(carta);
    }else if(recursoUso == 'recurso2'){
        carta = mazorobo.splice(2,1);
        mazorobo.push(carta);
    }else{}
    
    
    
    for(var i=0; i<=4; i++){
        if(recursoUso == 'recursof'+i+''){
        
            carta = mazorobo.splice(0,1);
            mazorobo.push(carta);
            
            carta = mazorobo.splice(0,1);
            mazorobo.push(carta);
            
            carta = mazorobo.splice(0,1);
            mazorobo.push(carta);
        
            break;
        }else{}
    }
    
    for(var k=0;k<=2;k++){
    
    document.getElementById('recurso'+k+'').innerHTML =mazorobo[k];    
    }
    
    //pintar color de recurso de div.
    colorRecurso(mazorobo);
    
}


//tablero
  var tablero = [["0","0","0","0"],
                 ["0","0","0","0"],
                 ["0","0","0","0"],
                 ["0","0","0","0"]];

  var puntaje = [["0","0","0","0","0","0"],
                 ["0","0","0","0","0","0"],
                 ["0","0","0","0","0","0"],
                 ["0","0","0","0","0","0"],
                 ["0","0","0","0","0","0"],
                 ["0","0","0","0","0","0"]];

var ref = ['','','','','','','','','','','',''], refitem = ['','','','','','','','','','','',''];

//MATRIZ CON LAS QUE HAREMOS LAS OPERACIONES.
function operacionesTablero(item){   
       
        var s=0;
        var q=0;
      
    for(var h=1; h<=16; h++){
        if(item == 'item'+h+''){
            
           tablero[s][q] =  recursoEnTablero;
            break;
      }else{
            if((h==4 || h==8 || h==12) && (q==3)){
                q=0;
                s++;
            }else{
            q++;
            }
      }
        
    }
}

function ConstruirEdificio(edificio){
    
    var cabaña = 'btn1', pasteleria='btn2', asilo='btn3', fabrica='btn4', pozo='btn5', capilla='btn6', granja='btn7';
    var M = 'Madera', P = 'Piedra', T = 'Trigo', L = 'Ladrillo', V = 'Vidrio';
    
    //Al finalizar CREAR UN FOR QUE PINTE LOS BBORDES, POR SI SE APLASTA A DOS BOTONES O MÁS .
    
    switch(edificio){
           
        case pozo:
            //¿para que la variable cons? ES PARA INDICAR A QUE EDIFICIO VAMOS A CONSTRUIR EN FUNCIÓN PONERRECURSO()
           cons = 0; //POR SI SE DA MAS DE UNA VEZ CLIC EN BOTON    
           cons++; // en ConstruirPozo Removemos la condición if.
           
        var suma = 1;    //RESALTA LOS BORDES CON DOTTED
        var activaPozo = 0;
        for(var i=0; i<=3; i++){
        
        for(var j=0; j<=2; j++){
            
            
            //PATRON 1
            if(tablero[i][j] == M && tablero[i][j+1] == P){
                activaPozo++;
            document.getElementById('item'+(suma)+'').style.border = '5px dotted white';
            document.getElementById('item'+(suma+1)+'').style.border = '5px dotted white';    
                
            
            //PATRON 2
            }else if(tablero[i][j] == P && tablero[i][j+1] == M){
                activaPozo++;
            document.getElementById('item'+(suma)+'').style.border = '5px dotted white';
            document.getElementById('item'+(suma+1)+'').style.border = '5px dotted white';    
                
           
            
            }else{}
            suma++;
        }
            suma++;
        }
            
            //reinicio variable suma
            suma = 1;
        for(var i=0; i<=2; i++){
        
        for(var j=0; j<=3; j++){
            //PATRON 3
             if(tablero[i][j] == M && tablero[i+1][j] == P){
                 activaPozo++;
            document.getElementById('item'+(suma)+'').style.border = '5px dotted white';
            document.getElementById('item'+(suma+4)+'').style.border = '5px dotted white';    
                
            
            //PATRON 4
            }else if(tablero[i][j] == P && tablero[i+1][j] == M){
                activaPozo++;
            document.getElementById('item'+(suma)+'').style.border = '5px dotted white';
            document.getElementById('item'+(suma+4)+'').style.border = '5px dotted white';    
                
           
            
            }else{}
            
            suma++;
        }
            
        }
        if(activaPozo == 0){alert('Debe completar alguno de los patrones respectivos a este edificio.');}else{alert('Seleccione alguno de los campos punteados para construir el edificio.');}    
            
        //reinicio variable suma
            suma = 1;    
           break;
            
            
        case cabaña:
        
            cons = 0;
            cons+=2;
            var activaCabaña = 0;
            var a1 = 2;
            for(var p=1; p<=3; p++){
            
            for(var k=0; k<=2; k++){
             
                
                //patron 1
                if(tablero[p-1][k+1] == T && tablero[p][k] == L && tablero[p][k+1] == V){
               activaCabaña++;
                document.getElementById('item'+(a1)+'').style.border = '5px dotted white';
                    
                document.getElementById('item'+(a1+3)+'').style.border = '5px dotted white';
                    
                document.getElementById('item'+(a1+4)+'').style.border = '5px dotted white';    
                    
                
                    
                    //patron 5
                }else if(tablero[p-1][k+1] == L && tablero[p][k] == T && tablero[p][k+1] == V){
                    activaCabaña++;
                document.getElementById('item'+(a1)+'').style.border = '5px dotted white';
                    
                document.getElementById('item'+(a1+3)+'').style.border = '5px dotted white';
                    
                document.getElementById('item'+(a1+4)+'').style.border = '5px dotted white';    
                
                    
                    //patron 4    
                }else if(tablero[p-1][k] == L && tablero[p-1][k+1] == V && tablero[p][k+1] == T){
                    activaCabaña++;
                document.getElementById('item'+(a1)+'').style.border = '5px dotted white';
                    
                document.getElementById('item'+(a1-1)+'').style.border = '5px dotted white';
                    
                document.getElementById('item'+(a1+4)+'').style.border = '5px dotted white';    
                    
                    //patron 7
                }else if(tablero[p-1][k] == T && tablero[p-1][k+1] == V && tablero[p][k+1] == L){
                    activaCabaña++;
                document.getElementById('item'+(a1)+'').style.border = '5px dotted white';
                    
                document.getElementById('item'+(a1-1)+'').style.border = '5px dotted white';
                    
                document.getElementById('item'+(a1+4)+'').style.border = '5px dotted white';    
                    
                    
                    //patron 2
                }else if(tablero[p-1][k] == T && tablero[p][k] == V && tablero[p][k+1] == L){
                    activaCabaña++;
                document.getElementById('item'+(a1-1)+'').style.border = '5px dotted white';
                    
                document.getElementById('item'+(a1+3)+'').style.border = '5px dotted white';
                    
                document.getElementById('item'+(a1+4)+'').style.border = '5px dotted white';    
                    
                    
                 //patron 6    
                }else if(tablero[p-1][k] == L && tablero[p][k] == V && tablero[p][k+1] == T){
                    activaCabaña++;
                document.getElementById('item'+(a1-1)+'').style.border = '5px dotted white';
                    
                document.getElementById('item'+(a1+3)+'').style.border = '5px dotted white';
                    
                document.getElementById('item'+(a1+4)+'').style.border = '5px dotted white';    
                    
                
                //patron 3
                }else if(tablero[p-1][k] == V && tablero[p-1][k+1] == L && tablero[p][k] == T){
                    activaCabaña++;
                document.getElementById('item'+(a1-1)+'').style.border = '5px dotted white';
                    
                document.getElementById('item'+(a1)+'').style.border = '5px dotted white';
                    
                document.getElementById('item'+(a1+3)+'').style.border = '5px dotted white';    
                 
                    
                //patron 8    
                }else if(tablero[p-1][k] == V && tablero[p-1][k+1] == T && tablero[p][k] == L){
                    activaCabaña++;
                document.getElementById('item'+(a1-1)+'').style.border = '5px dotted white';
                    
                document.getElementById('item'+(a1)+'').style.border = '5px dotted white';
                    
                document.getElementById('item'+(a1+3)+'').style.border = '5px dotted white';    
                    
                }else{}
                
                a1++;
            }
                a1++;
            }
            
            if(activaCabaña == 0){alert('Debe completar alguno de los patrones respectivos a este edificio.');}else{alert('Seleccione alguno de los campos punteados para construir el edificio.');}
            
            a1 = 2;
        break;
            
        case granja:
            
            cons = 0;
            cons+=3;
            var activaGranja = 0;
            var a2 = 2;
            
            for(var p=1; p<=3; p++){
            
            for(var k=0; k<=2; k++){
            
                //PATRON 1
            if(tablero[p-1][k] == T && tablero[p-1][k+1] == T && tablero[p][k] == M && tablero[p][k+1] == M){
               activaGranja++;
             document.getElementById('item'+a2+'').style.border = '5px dotted white';   
            document.getElementById('item'+(a2-1)+'').style.border = '5px dotted white';
            document.getElementById('item'+(a2+3)+'').style.border = '5px dotted white';
            document.getElementById('item'+(a2+4)+'').style.border = '5px dotted white';    
                
                //PATRON 2
            }else if(tablero[p-1][k] == T && tablero[p-1][k+1] == M && tablero[p][k] == T && tablero[p][k+1] == M){
                     activaGranja++;
            document.getElementById('item'+a2+'').style.border = '5px dotted white';   
            document.getElementById('item'+(a2-1)+'').style.border = '5px dotted white';
            document.getElementById('item'+(a2+3)+'').style.border = '5px dotted white';
            document.getElementById('item'+(a2+4)+'').style.border = '5px dotted white';    
                
                     
                     //PATRON 3
            }else if(tablero[p-1][k] == M && tablero[p-1][k+1] == M && tablero[p][k] == T && tablero[p][k+1] == T){
                     activaGranja++;
            document.getElementById('item'+a2+'').style.border = '5px dotted white';   
            document.getElementById('item'+(a2-1)+'').style.border = '5px dotted white';
            document.getElementById('item'+(a2+3)+'').style.border = '5px dotted white';
            document.getElementById('item'+(a2+4)+'').style.border = '5px dotted white';    
                
                     
                   //PATRON 4  
            }else if(tablero[p-1][k] == M && tablero[p-1][k+1] == T && tablero[p][k] == M && tablero[p][k+1] == T){
                    activaGranja++; 
            document.getElementById('item'+a2+'').style.border = '5px dotted white';   
            document.getElementById('item'+(a2-1)+'').style.border = '5px dotted white';
            document.getElementById('item'+(a2+3)+'').style.border = '5px dotted white';
            document.getElementById('item'+(a2+4)+'').style.border = '5px dotted white';         
                
                     
            }else{} 
             
               a2++ 
                
            }
                a2++;
        }
            
        if(activaGranja == 0){alert('Debe completar alguno de los patrones respectivos a este edificio.');}else{alert('Seleccione alguno de los campos punteados para construir el edificio.');}    
          
        a2 = 2; 
            
            
        break; 
            
        case asilo:
            cons = 0;
            cons+=4;
            var a3 = 3;
            var activaAsilo = 0;
             for(var i=0; i<=3; i++){
        
             for(var j=0; j<=1; j++){
            
            //PATRON 1
            if(tablero[i][j] == P && tablero[i][j+1] == P && tablero[i][j+2] == V){
             activaAsilo++;
document.getElementById('item'+a3+'').style.border = '5px dotted white';
document.getElementById('item'+(a3-1)+'').style.border = '5px dotted white';
document.getElementById('item'+(a3-2)+'').style.border = '5px dotted white';                
                
                //PATRON 2
             }else if(tablero[i][j] == V && tablero[i][j+1] == P && tablero[i][j+2] == P){
                activaAsilo++;
document.getElementById('item'+a3+'').style.border = '5px dotted white';
document.getElementById('item'+(a3-1)+'').style.border = '5px dotted white';
document.getElementById('item'+(a3-2)+'').style.border = '5px dotted white';                 
                      
            }else{}
                      
                 a3++;
             }
                      
                 a3+=2;
                 
             }
            
        a3 = 1;
            
           
        for(var i=0; i<=1; i++){
        
        for(var j=0; j<=3; j++){    
                //PATRON 3     
            if(tablero[i][j] == V && tablero[i+1][j] == P && tablero[i+2][j] == P){
               activaAsilo++;
            document.getElementById('item'+a3+'').style.border = '5px dotted white';
            document.getElementById('item'+(a3+4)+'').style.border = '5px dotted white';
            document.getElementById('item'+(a3+8)+'').style.border = '5px dotted white';    
                
                
                //PATRON 4
            }else if(tablero[i][j] == P && tablero[i+1][j] == P && tablero[i+2][j] == V){
                activaAsilo++;
            document.getElementById('item'+a3+'').style.border = '5px dotted white';
            document.getElementById('item'+(a3+4)+'').style.border = '5px dotted white';
            document.getElementById('item'+(a3+8)+'').style.border = '5px dotted white';    
                
                
                
            }else{}
               a3++;
            
           }
    }
            if(activaAsilo == 0){alert('Debe completar alguno de los patrones respectivos a este edificio.');}else{alert('Seleccione alguno de los campos punteados para construir el edificio.');}
            a3 = 3;
            
        break;
            
        case capilla:
            cons = 0;
            cons+=5;
            var p3a4 = 3;
            var p3a5 = 5;
            var p4a4=3;
            var p4a5 = 7;
            var p1a4 = 7;
            var p1a5 = 3;
            var p2a4 = 7;
            var p2a5 = 1;
            var activaCapilla = 0;
        for(var p=1; p<=3; p++){
        
        for(var k=0; k<=1; k++){
        //patron 3    
            
        if(tablero[p-1][k] == P && tablero[p-1][k+1] == V && tablero[p-1][k+2] == P && tablero[p][k] == V){    
           activaCapilla++;
        document.getElementById('item'+p3a4+'').style.border = '5px dotted white'; //p 3ra
        document.getElementById('item'+(p3a4-1)+'').style.border = '5px dotted white'; // v 2da
        document.getElementById('item'+(p3a4-2)+'').style.border = '5px dotted white'; //p 1ra
        document.getElementById('item'+p3a5+'').style.border = '5px dotted white';    // v inferior
            
            //patron 4
        }else if(tablero[p-1][k] == P && tablero[p-1][k+1] == V && tablero[p-1][k+2] == P && tablero[p][k+2] == V){
                 activaCapilla++;
                 
            document.getElementById('item'+p4a4+'').style.border = '5px dotted white'; //p 3ra
        document.getElementById('item'+(p4a4-1)+'').style.border = '5px dotted white'; // v 2da
        document.getElementById('item'+(p4a4-2)+'').style.border = '5px dotted white'; //p 1ra
        document.getElementById('item'+p4a5+'').style.border = '5px dotted white';    // v inferior
            
                 //patron 1
        }else if(tablero[p-1][k+2] == V && tablero[p][k] == P && tablero[p][k+1] == V && tablero[p][k+2] == P){
                 activaCapilla++;
                 
         document.getElementById('item'+p1a4+'').style.border = '5px dotted white'; //p 3ra
        document.getElementById('item'+(p1a4-1)+'').style.border = '5px dotted white'; // v 2da
        document.getElementById('item'+(p1a4-2)+'').style.border = '5px dotted white'; //p 1ra
        document.getElementById('item'+p1a5+'').style.border = '5px dotted white';    // v inferior
            
                 //patron 2
        }else if(tablero[p-1][k] == V && tablero[p][k] == P && tablero[p][k+1] == V && tablero[p][k+2] == P){
                 activaCapilla++;
                 
         document.getElementById('item'+p2a4+'').style.border = '5px dotted white'; //p 3ra
        document.getElementById('item'+(p2a4-1)+'').style.border = '5px dotted white'; // v 2da
        document.getElementById('item'+(p2a4-2)+'').style.border = '5px dotted white'; //p 1ra
        document.getElementById('item'+p2a5+'').style.border = '5px dotted white';    // v inferior
            
                 
        }else{}
            p3a4++;
            p3a5++;
            p4a4++;
            p4a5++;
             p1a4++;
            p1a5++;
             p2a4++;
            p2a5++;
        }
           p3a4+=2;
            p3a5+=2;
            p4a4+=2;
            p4a5+=2;
            p1a4+=2;
            p1a5+=2;
            p2a4+=2;
            p2a5+=2;
       }
            
            
            
        var p5a4 = 2;
        var p5a5 = 9;    
        var p7a4 = 2;
        var p7a5 = 1;
        var p8a4 = 1;
        var p8a5 = 2;
        var p6a4 = 1;
        var p6a5 = 10;    
            
        for(var i=0; i<=1; i++){
        
        for(var l=1; l<=3; l++){    
            //patron 5    
        
        if(tablero[i][l] == P && tablero[i+1][l] == V && tablero[i+2][l] == P && tablero[i+2][l-1] == V){    
         activaCapilla++;
        document.getElementById('item'+p5a4+'').style.border = '5px dotted white';
        document.getElementById('item'+(p5a4+4)+'').style.border = '5px dotted white';
        document.getElementById('item'+(p5a4+8)+'').style.border = '5px dotted white'; 
        document.getElementById('item'+p5a5+'').style.border = '5px dotted white';    
            
            //patron 7
        }else if(tablero[i][l] == P && tablero[i+1][l] == V && tablero[i+2][l] == P && tablero[i][l-1] == V){    
         activaCapilla++;
        document.getElementById('item'+p7a4+'').style.border = '5px dotted white';
        document.getElementById('item'+(p7a4+4)+'').style.border = '5px dotted white';
        document.getElementById('item'+(p7a4+8)+'').style.border = '5px dotted white'; 
        document.getElementById('item'+p7a5+'').style.border = '5px dotted white';    
            
            //patron 8
        }else if(tablero[i][l-1] == P && tablero[i+1][l-1] == V && tablero[i+2][l-1] == P && tablero[i][l] == V){    
         activaCapilla++;
        document.getElementById('item'+p8a4+'').style.border = '5px dotted white';
        document.getElementById('item'+(p8a4+4)+'').style.border = '5px dotted white';
        document.getElementById('item'+(p8a4+8)+'').style.border = '5px dotted white'; 
        document.getElementById('item'+p8a5+'').style.border = '5px dotted white';    
            
            //patron 6
        }else if(tablero[i][l-1] == P && tablero[i+1][l-1] == V && tablero[i+2][l-1] == P && tablero[i+2][l] == V){    
         activaCapilla++;
        document.getElementById('item'+p6a4+'').style.border = '5px dotted white';
        document.getElementById('item'+(p6a4+4)+'').style.border = '5px dotted white';
        document.getElementById('item'+(p6a4+8)+'').style.border = '5px dotted white'; 
        document.getElementById('item'+p6a5+'').style.border = '5px dotted white';    
            
            
        }else{}
            p5a4++;
            p5a5++;
            p7a4++;
            p7a5++;
            p8a4++;
            p8a5++;
            p6a4++;
            p6a5++;
        }
            p5a4++;
            p5a5++;
            p7a4++;
            p7a5++;
            p8a4++;
            p8a5++;
            p6a4++;
            p6a5++;
        }
            
        if(activaCapilla == 0){alert('Debe completar alguno de los patrones respectivos a este edificio.');}else{alert('Seleccione alguno de los campos punteados para construir el edificio.');}    
            
            
        break;
            
        case fabrica:
            cons = 0;
            cons+=6;
            var activaFabrica = 0;
        var f1a = 8;
        var f1b = 1;    
            
            var z=0;
        for(var p=1; p<=3; p++){    
            
            //patron1
            if(tablero[p-1][z] == M && tablero[p][z] == L && tablero[p][z+1] == P && tablero[p][z+2] == P && tablero[p][z+3] == L){
            activaFabrica++;
    document.getElementById('item'+f1b+'').style.border = '5px dotted white';
    document.getElementById('item'+f1a+'').style.border = '5px dotted white';            
    document.getElementById('item'+(f1a-1)+'').style.border = '5px dotted white';
    document.getElementById('item'+(f1a-2)+'').style.border = '5px dotted white';
    document.getElementById('item'+(f1a-3)+'').style.border = '5px dotted white';            
                
                
            
            }else{}
         
            f1a+=4;
            f1b+=4;
            
        }
            
            
    //===============patron 2
            
        var p2a = 8;
        var p2b = 4;    
            
        for(var p=1; p<=3; p++){    
            var k=0;
            //patron2
            if(tablero[p-1][k+3] == M && tablero[p][k] == L && tablero[p][k+1] == P && tablero[p][k+2] == P && tablero[p][k+3] == L){
            activaFabrica++;
    document.getElementById('item'+p2b+'').style.border = '5px dotted white';
    document.getElementById('item'+p2a+'').style.border = '5px dotted white';            
    document.getElementById('item'+(p2a-1)+'').style.border = '5px dotted white';
    document.getElementById('item'+(p2a-2)+'').style.border = '5px dotted white';
    document.getElementById('item'+(p2a-3)+'').style.border = '5px dotted white';            
                
                
            
            }else{}
            p2a+=4;
            p2b+=4;    
        }
            
//======================================patron 3            
         
            
        var p3a = 4;
        var p3b = 8;    
            
        for(var p=1; p<=3; p++){    
            var k=0;
           
            if(tablero[p-1][k] == L && tablero[p-1][k+1] == P && tablero[p-1][k+2] == P && tablero[p-1][k+3] == L && tablero[p][k+3] == M){
            activaFabrica++;
    document.getElementById('item'+p3b+'').style.border = '5px dotted white';
    document.getElementById('item'+p3a+'').style.border = '5px dotted white';            
    document.getElementById('item'+(p3a-1)+'').style.border = '5px dotted white';
    document.getElementById('item'+(p3a-2)+'').style.border = '5px dotted white';
    document.getElementById('item'+(p3a-3)+'').style.border = '5px dotted white';            
                
                
            
            }else{}
            p3a+=4;
            p3b+=4;    
        }
            
            
//==============================PATRONO 4
            
         var p4a = 4;
        var p4b = 5;    
            
        for(var p=1; p<=3; p++){    
            var k=0;
            
            if(tablero[p-1][k] == L && tablero[p-1][k+1] == P && tablero[p-1][k+2] == P && tablero[p-1][k+3] == L && tablero[p][k] == M){
            activaFabrica++;
    document.getElementById('item'+p4b+'').style.border = '5px dotted white';
    document.getElementById('item'+p4a+'').style.border = '5px dotted white';            
    document.getElementById('item'+(p4a-1)+'').style.border = '5px dotted white';
    document.getElementById('item'+(p4a-2)+'').style.border = '5px dotted white';
    document.getElementById('item'+(p4a-3)+'').style.border = '5px dotted white';            
                
                
            
            }else{}
              p4a+=4;
            p4b+=4;  
        }   
         
            
//===========================patron 5
          var p5a =2;
            var p5b = 13;
            
        for(var l=1; l<=3; l++){
           var i=0;
             //patron 5
             if(tablero[i][l] == L && tablero[i+1][l] == P && tablero[i+2][l] == P && tablero[i+3][l] == L && tablero[i+3][l-1] == M){    
            activaFabrica++;
    document.getElementById('item'+p5b+'').style.border = '5px dotted white';
    document.getElementById('item'+p5a+'').style.border = '5px dotted white';             
    document.getElementById('item'+(p5a+4)+'').style.border = '5px dotted white';
    document.getElementById('item'+(p5a+8)+'').style.border = '5px dotted white';
    document.getElementById('item'+(p5a+12)+'').style.border = '5px dotted white';             
                 
                 
             }else{}
            
            p5a++; p5b++;
        }
            
            
 //===========================PATRON 6
        var p6a = 2, p6b = 1;
        for(var l=1; l<=3; l++){
           var i=0;    
            
        if(tablero[i][l] == L && tablero[i+1][l] == P && tablero[i+2][l] == P && tablero[i+3][l] == L && tablero[i][l-1] == M){
            activaFabrica++;
    document.getElementById('item'+p6b+'').style.border = '5px dotted white';
    document.getElementById('item'+p6a+'').style.border = '5px dotted white';             
    document.getElementById('item'+(p6a+4)+'').style.border = '5px dotted white';
    document.getElementById('item'+(p6a+8)+'').style.border = '5px dotted white';
    document.getElementById('item'+(p6a+12)+'').style.border = '5px dotted white';        
            
            
        }else{}
            
            p6a++; p6b++;
        }
            
//==================================PATRON 7
        var p7a = 1, p7b = 2;   
            
        for(var l=1; l<=3; l++){
           var i=0;    
            
        if(tablero[i][l-1] == L && tablero[i+1][l-1] == P && tablero[i+2][l-1] == P && tablero[i+3][l-1] == L && tablero[i][l] == M){
            activaFabrica++;
    document.getElementById('item'+p7b+'').style.border = '5px dotted white';
    document.getElementById('item'+p7a+'').style.border = '5px dotted white';             
    document.getElementById('item'+(p7a+4)+'').style.border = '5px dotted white';
    document.getElementById('item'+(p7a+8)+'').style.border = '5px dotted white';
    document.getElementById('item'+(p7a+12)+'').style.border = '5px dotted white';            
            
            
        }else{}
             p7a++; p7b++;
        }
            
            
//=================================PATRON 8
      var p8a = 1, p8b = 14;
            
        for(var l=1; l<=3; l++){
           var i=0;
          
        if(tablero[i][l-1] == L && tablero[i+1][l-1] == P && tablero[i+2][l-1] == P && tablero[i+3][l-1] == L && tablero[i+3][l] == M){
            activaFabrica++;
        document.getElementById('item'+p8b+'').style.border = '5px dotted white';
    document.getElementById('item'+p8a+'').style.border = '5px dotted white';             
    document.getElementById('item'+(p8a+4)+'').style.border = '5px dotted white';
    document.getElementById('item'+(p8a+8)+'').style.border = '5px dotted white';
    document.getElementById('item'+(p8a+12)+'').style.border = '5px dotted white'; 
            
            
        }else{}
            p8a++; p8b++;
        }
            
        if(activaFabrica == 0){alert('Debe completar alguno de los patrones respectivos a este edificio.');}else{alert('Seleccione alguno de los campos punteados para construir el edificio.');}    
            
        break;
            
        case pasteleria:
            cons = 0;
            cons+=7;
            var activaPasteleria = 0;
            //=================PATRON 1
        var pp1a= 7, pp1b = 2;
        var pp2a = 3, pp2b = 6;    
        for(var p=1; p<=3; p++){
        
        for(var k=0; k<=1; k++){
            
        if(tablero[p-1][k+1] == T && tablero[p][k] == L && tablero[p][k+1] == V && tablero[p][k+2] == L){
            activaPasteleria++;
        document.getElementById('item'+pp1a+'').style.border = '5px dotted white'; //
        document.getElementById('item'+(pp1a-1)+'').style.border = '5px dotted white'; // 
        document.getElementById('item'+(pp1a-2)+'').style.border = '5px dotted white'; //
        document.getElementById('item'+pp1b+'').style.border = '5px dotted white';    // 
            
            //patron 2
        }else if(tablero[p-1][k] == L && tablero[p-1][k+1] == V && tablero[p-1][k+2] == L && tablero[p][k+1] == T){
        activaPasteleria++;
        document.getElementById('item'+pp2a+'').style.border = '5px dotted white'; //
        document.getElementById('item'+(pp2a-1)+'').style.border = '5px dotted white'; // 
        document.getElementById('item'+(pp2a-2)+'').style.border = '5px dotted white'; //
        document.getElementById('item'+pp2b+'').style.border = '5px dotted white';    //    
            
                 
                 
          }else{}
          
                 pp1a++; pp1b++;
                 pp2a++; pp2b++;
        }
            pp1a+=2; pp1b+=2;
            pp2a+=2; pp2b+=2;
        
        }
         
        //=====================PATRON 3
        var pp3a = 2, pp3b = 5;
        var pp4a = 1, pp4b = 6;    
        for(var i=0; i<=1; i++){
        
        for(var l=1; l<=3; l++){
            
        if(tablero[i][l] == L && tablero[i+1][l] == V && tablero[i+2][l] == L && tablero[i+1][l-1] == T){ 
            activaPasteleria++;
        document.getElementById('item'+pp3a+'').style.border = '5px dotted white';
        document.getElementById('item'+(pp3a+4)+'').style.border = '5px dotted white';
        document.getElementById('item'+(pp3a+8)+'').style.border = '5px dotted white'; 
        document.getElementById('item'+pp3b+'').style.border = '5px dotted white';    
            
            
        //======patron 4
        }else if(tablero[i][l-1] == L && tablero[i+1][l-1] == V && tablero[i+2][l-1] == L && tablero[i+1][l] == T){  
         activaPasteleria++;
        document.getElementById('item'+pp4a+'').style.border = '5px dotted white';
        document.getElementById('item'+(pp4a+4)+'').style.border = '5px dotted white';
        document.getElementById('item'+(pp4a+8)+'').style.border = '5px dotted white'; 
        document.getElementById('item'+pp4b+'').style.border = '5px dotted white';         
                 
                 
        }else{}
           pp3a++; pp3b++;
           pp4a++; pp4b++;    
        }
            pp3a++; pp3b++;
            pp4a++; pp4b++;
        }
            
        if(activaPasteleria == 0){alert('Debe completar alguno de los patrones respectivos a este edificio.');}else{alert('Seleccione alguno de los campos punteados para construir el edificio.');}    
            
        break;    
            
        default:
    
    }
}